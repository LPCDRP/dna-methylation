---
title: "Isolate-specific DNA base modification report"
author: "Laboratory for Pathogenesis of Clinical Drug Resistance and Persistence"
output:
  html_document:
    css: report.css
    fig_caption: yes
    fig_width: 8
    keep_md: yes
    smart: no
    theme: readable
    toc: yes
  pdf_document:
    toc: yes
  word_document:
    fig_caption: yes
---


```{r, echo=FALSE, message=F, warning=F}
##### Load in packages and set themes #####

library(ggplot2)
library(ggthemes)
library(plyr)
library(stringr) # For manipulating strings
library('data.table')
library(pander)

# Qualitative color schemes by Paul Tol. Use the number corresponding to the nuber of different colored items that will be plotted
#tol1qualitative=c("#4477AA")
#tol2qualitative=c("#4477AA", "#CC6677")
tol3qualitative=c("#4477AA", "#DDCC77", "#CC6677")
tol4qualitative=c("#4477AA", "#117733", "#DDCC77", "#CC6677")
alt_tol4qualitative=c("#332288", "#44AA99", "#661100", "#AA4466")

theme_set(theme_tufte(base_size = 10, base_family = "Helvetica")) # this can be changed based on aesthetic preference. See ggthemes() documentation for examples of different styles
```

```{r, echo=FALSE, message=F, warning=F}
#commandArgs takes in user input
#from the shell (or a makefile), call this script like this:
#Rscript graphical-output.R  $GROUPHOME/data/depot/assembly/$isolate/methylation/data/modifications.csv.gz /grp/valafar/data/genomes/$isolate.fasta path/to/directory/where/plots/should/be/written
# This is currently implemented in findmethyls.mk

##### Load in data #####

## temporary hard-coding for testing -- remove

iso <- read.table('/grp/valafar/data/depot/assembly/2-0046/methylation/data/modifications.csv.gz', header = TRUE, sep = ',', stringsAsFactors=F)
isofavector <- scan(file='/grp/valafar/data/genomes/2-0046.fasta', what=character(), skip = 1) #note each element is a 60 bp character vector

#options <- commandArgs(trailingOnly = TRUE)
#iso <- read.table(options[1], header = TRUE, sep = ',', stringsAsFactors=F)

#isofavector <- scan(file=options[2], what=character(), skip = 1) #note each element is a 60 bp character vector
isofasta <- paste0(isofavector, collapse='') #this concatenates the fasta input into a single string for easy substring indexing

#setwd(options[3])

###### Preprocess data ######

iso$ipdRatiolog10 <- log10(iso$ipdRatio) # log-transform the ipdRatios

# calculate standard deviation for each base
iso$sd <- (iso$ipdRatiolog10-mean(iso$ipdRatiolog10))/sd(iso$ipdRatiolog10)

# Absolute value of standard deviations
iso$sd_abs <- abs(iso$sd)

### segregate into groupings ###

### by IPDRatio ###

iso$group_IPD <- cut(iso$ipdRatio,
                     breaks=c(0.25,seq(0.75,15.25,by=0.5),15.75), 
                     labels=c(".5","1","1.5","2","2.5","3","3.5","4","4.5","5","5.5","6","6.5","7","7.5","8","8.5","9","9.5","10","10.5","11","11.5","12","12.5","13","13.5","14","14.5","15","15.5" ))

### by coverage ###

iso$cov_group <- cut(iso$coverage,
                     breaks=c(min(iso$coverage),seq((min(iso$coverage)+5),(max(iso$coverage)-5), by=5), max(iso$coverage)), 
                     labels=c(seq(min(iso$coverage),(max(iso$coverage-5)),by=5)))

### by modelPrediction ###
iso$modpred_group <- cut(as.integer(iso$modelPrediction),
                         breaks=c(0,seq(1,29,by=1),30), 
                         labels=c(seq(0,29,by=1)))

#sd(iso$sd)
iso_t <- data.table(iso)
sd_adj_factor <- iso_t[,list(mean(sd),sd_cov=sd(sd)),by=cov_group]
iso_t$sd_adj <- iso_t$sd/sd_adj_factor$sd_cov[iso_t$cov_group]

iso <- iso_t

### Identify bases that are candidate methylated motifs ###

mamA <- c("CTGGAG","CTCCAG")

a <- str_locate_all(isofasta, mamA) # Finds all instances of mamA within the sequence
A <- sort(rbind(c(a[[1]][,1],a[[2]][,1]))) 
MamAc <- A*2 + 2 # This adjusts to give the modified base
MamA <- A*2 + 7 # in 

iso$mamA <- 0
iso$mamA[MamA] <- 1
iso$mamA[MamAc] <- 1
iso$mamA <- as.factor(iso$mamA) 

# mamB motif

MamB <- "CACGCAG"
MamBc <- "CTGCGTG" #The complement

b1 <- str_locate_all(isofasta, MamB)
B1<- b1[[1]][,1] 
B1 <- B1*2 + 9

b2 <- str_locate_all(isofasta, MamBc)
B2 <- b2[[1]][,1] 
B2 <- B2*2 + 2 

MamB <- unique(sort(rbind(B1, B2)))

iso$mamB <- 0
iso$mamB[MamB] <- 1
iso$mamB <- as.factor(iso$mamB) 

#hsdM motif

hsdM1 <- "GAT[ACTG][ACTG][ACTG][ACTG][AG]TAC"
hsdM2 <- "GTA[TC][ACTG][ACTG][ACTG][ACTG]ATC"

h <- str_locate_all(isofasta, hsdM1)
H1 <- h[[1]][,1]
H1p <- H1*2 + 1
H1n <- H1*2 + 16 

h <- str_locate_all(isofasta, hsdM2)
H2 <-  h[[1]][,1]
H2p <- H2*2 + 3
H2n <- H2*2 + 18 

HsdM <- unique(sort(rbind(H1p, H1n, H2n, H2p)))

iso$hsdM <- 0
iso$hsdM[HsdM] <- 1
iso$hsdM <- as.factor(iso$hsdM) 

# Create column for indicating if base is target base within one of the motifs, and which MTase methylates that motif

iso$MTase_motif <- "none"
iso$MTase_motif[iso$hsdM == "1"] <- "hsdM"
iso$MTase_motif[iso$mamA == "1"] <- "mamA"
iso$MTase_motif[iso$mamB == "1"] <- "mamB"
iso$MTase_motif <- as.factor(iso$MTase_motif) 

### Create subsets of bases above certain parameter -- ideally adapted to scale based on inferred distributions, or to be set from the command-line in the Makefile ### #! WIP #!
iso_surprisers_non_motif <- iso[iso$sd_adj >= -qnorm(1/length(iso$tpl)) & iso$MTase_motif == "none", ]
iso_known_motif <- iso[iso$MTase_motif != "none", ] # Make separate data frame only with known motifs
iso_methcomp <- iso[iso$MTase_motif == "none", ] # Make separate data frame only lacking known motifs

# Check for bias due to modelPrediction
#z + geom_boxplot(aes(iso_known_motif$modelPrediction,iso_known_motif$ipdRatiolog10, group = cut_width(modelPrediction, 0.5)))

##### Setting subsamples for statistics and plot generation #####

#subsample of just ostensibly unmethylated (non-known motif -- some may be actually methylated) bases:
sample_unmeth <- iso_methcomp[sample(nrow(iso_methcomp),100000),]

#Subsample of all bases:
sample_all <- iso[sample(nrow(iso),100000),]

```

## General stats for QC/diagnostics 

The average single-strand coverage for this isolate is `r mean(iso$coverage)` 

```{r, echo=FALSE, message=F, warning=F}
pander(summary(iso$MTase_motif))
```

The above table shows the number of motifs for each MTase across the genome, irrespective of modification status. These should generally range between ~3850-3925 for mamA, ~810-840 for mamB, and ~690-730 for hsdM, according to [prior public work on SMRT-sequence *M. tuberculosis* sequences](https://www.nature.com/articles/s41598-017-18188-y/tables/1). Further deviation from these numbers suggests an error in indexing, or an incomplete genome.

## Tables of IPD Ratios by MTase

### Summary statistics of coverage-adjusted standard deviations for each motif 
```{r, echo=FALSE, message=F, warning=F}
a <- do.call(rbind , by(iso$sd_adj, iso$MTase_motif, summary))
pander(a)
```

### Summary statistics of raw IPD Ratio for each motif 
```{r, echo=FALSE, message=F, warning=F}
b <- do.call(rbind , by(iso$ipdRatio, iso$MTase_motif, summary))
pander(b)
```
## Graphical Output 

```{r, echo=FALSE, message=F, warning=F,fig.align='center', fig.cap="\\label{fig:figs}IPD Ratio by base"}
IPD_base_distribution <- ggplot(data=iso, aes(sd_adj, na.rm=TRUE, fill = base)) + geom_density(alpha=0.3) + scale_fill_manual(values=tol4qualitative) +
  #stat_smooth(method = "gam", formula = y ~ x, se = TRUE, level = 0.99) +
  ggtitle('IPD Ratio (# st. dev. from mean) by base') +
  theme(text = element_text(size=20)) +
  ylab('density') +
  xlab('sd of log10 of IPD Ratio')
print(IPD_base_distribution) 
```

Here, we expect the distributions for each base to be roughly the same, with a single mode, and a mean very close to 0 standard deviations. Any strong departure from these expected results likely indicate error in the raw data, or in one of the processing steps.

### IPD distribution by MTase motif
```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
IPD_MTase_distribution <- ggplot(data=iso, aes(sd_adj, na.rm=TRUE, fill = MTase_motif)) + geom_density(alpha=0.3) + scale_fill_manual(values=alt_tol4qualitative) +
  #stat_smooth(method = "gam", formula = y ~ x, se = TRUE, level = 0.99) +
  ggtitle('IPD Ratio (# st. dev. from mean) by MTase motif') +
  theme(text = element_text(size=20)) +
  ylab('density') +
  xlab('sd of log10 of IPD Ratio')
print(IPD_MTase_distribution) 
```

Here the distributiuons are separated by the motif they occupy. For any motif that is methylated consistently, we expect the center of its distribution to fall many standard deviations (>5 for m6A methylation) above 0. Smaller bumps in such distributions around 0 indicate a subset of the motifs for an isolate with an active MTase remain unmethylated. Distributions centered well-above 0 standard deviations but below 5 standard deviations indicate a heterogeneously active MTase in the isolate. The "none" distribution should always be centered around 0. Otherwise it is likely that an error is in the raw data or its processing. In isolates where an MTase in inactive, we expect the distribution to look very similar to the "none" distribution.

### High-IPD outliers by base

```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
surprise_plot <- ggplot(data=iso_surprisers_non_motif, aes(sd_adj, alpha = 0.1, na.rm=TRUE, fill = base))  + scale_fill_manual(values=tol4qualitative) +  geom_histogram() + facet_wrap(~ base) +
#stat_smooth(method = "gam", formula = y ~ x, se = TRUE, level = 0.99) +
  ggtitle('Histogram of high IPD bases colored by base') +
  theme(text = element_text(size=20)) +
  ylab('frequency') +
  xlab('standard deviations from mean log10 of IPD Ratio')
print(surprise_plot) 
```

The above graph shows a histogram of the number of bases at least 5 standard deviations above the mean that do not have a known motif. As of now, we do not know what to expect here, but this may be useful in identifying bases of interest to explore further in a given isolate, or perhaps across isolates.

### Standard deviations from mean by MTase 

```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
box_IPD_distribution <- ggplot(data=iso, aes(x=MTase_motif, sd_adj, na.rm=TRUE, fill = MTase_motif)) + geom_boxplot(alpha=0.3) + scale_fill_manual(values=alt_tol4qualitative) +
  #stat_smooth(method = "gam", formula = y ~ x, se = TRUE, level = 0.99) +
  ggtitle('IPD Ratio (# st. dev. from mean) by MTase motif') +
  theme(text = element_text(size=20)) +
  ylab('st. dev from mean of 10g10 IPD Ratio') +
  xlab('MTase motif')
print(box_IPD_distribution) 
```

The black line in the middle of each box plots depicts the median, while the dots outside of the box edges are outside the 10-90th% range. Dots very distant from the mean of otherwise active MTases (near zero) suggest site-specific hypomethylation of that base.

### Standard deviations from mean by base

```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
violin_IPD_base_distribution <- ggplot(data=iso, aes(x=base, sd_adj, na.rm=TRUE, fill = MTase_motif)) + geom_violin(alpha=0.3) + scale_fill_manual(values=alt_tol4qualitative) +
  #stat_smooth(method = "gam", formula = y ~ x, se = TRUE, level = 0.99) +
  ggtitle('IPD Ratio (# st. dev. from mean) by MTase motif') +
  theme(text = element_text(size=20)) +
  ylab('st. dev from mean of 10g10 IPD Ratio') +
  xlab('MTase motif')
print(violin_IPD_base_distribution) 
```

Since all three methyaltion motifs methylate adenine, the only base in this plot with multiple distributions (violins) should be A. If this is not the case, then the motif-base labels are applied incorrectly. Each of the MTase violins should be centered well-above 0 if active, and nearby 0 if inactive. If any of the non-adenine bases have "bubbles" above 0, this suggests possible errors, or, alternatively, possible non-adenine base modification.

```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
corr <- cor(iso$sd_abs,iso$coverage)
sd_coverage <- ggplot(data=sample_all, aes(coverage, sd_abs)) + geom_point(alpha=0.1) +
  stat_smooth(method = "glm", formula = y ~ x, se = TRUE, level = 0.99) +
  ggtitle('Absolute value of standard deviations from\nmean log10 IPD Ratio as a function of coverage') +
  theme(text = element_text(size=20)) +
  xlab('Coverage') +
  ylab('standard deviations from mean log10 IPD Ratio')
print(sd_coverage) 
```

The correlation between coverage and absolute standard deviations from the mean is `r corr`.

```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
MTase_qq_plot <- ggplot(iso, aes(sample = sd_adj)) + # subsample of all
  stat_qq(aes(color = MTase_motif)) + scale_color_manual(values=alt_tol4qualitative) +
  #scale_colour_manual(values=sigpalette) +
  ggtitle('Normal Q-Q Plot of subsample of all bases') +
  theme(text = element_text(size=20)) +
  geom_hline(yintercept = -qnorm(1/length(iso$tpl)) , color = "gray") +
  geom_hline(yintercept = -(-qnorm(1/length(iso$tpl))) , color = "gray") +
  xlab('theoretical quantiles') +
  ylab('sample quantiles') +
  geom_abline(intercept = 0, slope = 1) +
  facet_wrap(~MTase_motif)
print(MTase_qq_plot) 
```
In these "quantile-quantile" plots, all data points are ordered on the x-axis, and plotted by standard deviations from the mean on the y-axis. The horizontal lines indicate the number of standard deviations that would be expected to occur only once in a standard distribution. The diagonal line is the line that would be followed by a perfectly normal distribution. Deviations from this diagonal line suggests skewness away from a normal distribution. For MTases that are active, the points should follow a diagonal line well above the main line (centered at the median sds for that motif). Deviations below that line suggest hypomethylation or non-methylation ata subset of sites for the motif. Deviations above the diagonal line, for the "none" category suggest a mixture of stochastically high IPDs, or a subset of non-motif modifications. 


### standard deviations binned by coverage

```{r, echo=FALSE, message=F, warning=F,fig.align='center'}
coverage_box <- ggplot(data=iso, aes(factor(cov_group), sd_adj)) + geom_boxplot() +
  #stat_smooth(method = "loess", formula = y ~ x, se = TRUE, level = 0.999) +
  #geom_hline(yintercept = 1, color = "green") +
  geom_hline(yintercept = 0, color = "brown") +
  #geom_hline(yintercept = -1, color = "red") +
  geom_vline(xintercept = median(iso$coverage), color = "dodgerblue", linetype = "dashed") +
  #geom_vline(xintercept = 14.26, color = "dodgerblue", linetype = "dashed") +
  #geom_vline(xintercept = 5, color = "red", linetype = "dashed") +
  ggtitle('St. dev as a function of coverage') +
  theme(text = element_text(size=20)) +
  xlab('coverage') +
  ylab('standard deviations from the mean') +
  expand_limits(y=0)
print(coverage_box) 
```

This plot gives an overview of the distribution of coverage, and will highlight low-coverage areas that are particularly problematic. 

## Analysis by Groupings
