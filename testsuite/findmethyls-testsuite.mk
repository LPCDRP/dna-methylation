#Makefile to organize creation of mutlifasta file from methylation data
#from dna-methylation/testsuite/ call like this:
#make -f findmethyls-testsuite.mk

#Variables
ISOLATE = test
source_dir = precursers
source_csv = $(source_dir)/$(ISOLATE)-modifications.csv.gz
sourcemotifs = $(source_dir)/$(ISOLATE)-motifs.gff.gz
sourcegenbank = $(GROUPHOME)/data/annotation/2-0046.gbk
sourcebstats = $(source_dir)/base-stats-all-isolates.tsv
nproc ?= 6
source_fasta = $(source_dir)/$(ISOLATE).fasta

#Recipies
all : $(ISOLATE)/results.csv

$(ISOLATE)/results.csv : $(ISOLATE)/master-methylap.csv
	Rscript testmethlap.R $^ $@

$(ISOLATE)/master-methylap.csv : $(ISOLATE)/$(ISOLATE)_methylap.p
	../src/cucumbler.py -p $^ -o $@ -i $(ISOLATE) -l $(source_dir)/test-lineage.txt -d $(source_dir)/test-dst.txt

$(ISOLATE)/$(ISOLATE)_methylap.p : $(ISOLATE)/$(ISOLATE)-methyls.csv
	timeout 7200s python ../src/methylap.py -o $@ -m $^ -e $(sourcegenbank) -n $(nproc) -s ../targets/sigmotifs.txt -t ../targets/mtase-motifs.txt -g ../targets/g4quad-motifs.txt 

$(ISOLATE)/$(ISOLATE)-methyls.csv : $(ISOLATE)/$(ISOLATE)motifs.csv $(source_csv) $(source_fasta) $(sourcebstats)
	Rscript ../src/findmethyls.R $^ $(ISOLATE) $@

$(ISOLATE)/$(ISOLATE)motifs.csv : $(sourcemotifs) $(ISOLATE)
	python ../src/methylgffpreproc.py -i $(word 1,$^) -o $@

$(ISOLATE) : 
	mkdir -p $(ISOLATE)  
