#!/usr/bin/env python2.7

import csv
import argparse


parser = argparse.ArgumentParser(description="Writes iTOL compatable file to color isolates in phylogenetic tree."
                                             "Based on methylation status of specified epiallele.")
parser.add_argument("-e", "--epi", help="csv with methylation status of each isolate at epiallele",
                    default='/home/dconkleg/workspace/methylation/dna-methylation/itol/p_bioB_Stop_-254.csv')
parser.add_argument("-o", "--out", help="outfile, which can be dragged onto Webpage with iTOL phylogenetic Tree.",
                    default='/home/dconkleg/workspace/methylation/dna-methylation/itol/p_bioB_Stop_-254-colors.txt')
args = parser.parse_args()

itolines = []  # list stores lines of itol file to be written
with open(args.epi, 'r') as isolate_file:
    next(isolate_file)  # skip first line
    for line in isolate_file:
        cleanline = line.strip('\n')
        linebits = cleanline.split('\t')
        isolate = linebits[0]
        status = linebits[1]
        if len(isolate) > 10:
            if "SEA" in isolate:
                part1 = isolate[0:5]
                part2 = isolate[8:]
                isolate = part1 + part2
        if status == 'Hypomethylated':
            itolines.append(isolate + ' range #d95f02 ' + status)  # hypomethylated isolates are orange
        elif status == 'Methylated':
            itolines.append(isolate + ' range #7570b3 ' + status)  # methylated isolates are purple
        elif status == 'Indeterminate':
            itolines.append(isolate + ' range #1b9e77 ' + status)  # indeterminate isolates are green
        elif status == 'Not Applicable':
            itolines.append(isolate + ' range #808080 ' + status)  # not applicable isolates are grey
        else:
            print('error')

with open(args.out, 'w') as f:
    writer = csv.writer(f)
    writer.writerow(['TREE_COLORS'])
    writer.writerow(['SEPARATOR SPACE'])
    writer.writerow(['DATA'])
    for isolate in itolines:
        writer.writerow([isolate])
