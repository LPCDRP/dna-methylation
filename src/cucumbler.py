#!/usr/bin/env python2.7
import pickle
import argparse
import sqlite3
from sqlite3 import Error


def truesig(tsslist, sig):  # if SFBS motif in expected position for a TSS, returns that TSS. Otherwise returns False.
    expectedtss = False  # holds the TSS found in expected position. False by default.
    if tsslist:  # list will be false if empty
        for tssd in tsslist:
            if sig['S35start'] != 'none':
                if sig['strand'] == '+' and tssd['TSSstrand'] == '+' and sig['S35start'] <= tssd['TSSrelP']:
                    middledist = (abs(tssd['TSSrelP'] - sig['S35start']) + abs(tssd['TSSrelP'] - sig['S35end'])) / 2
                    if 30 <= middledist <= 40:
                        expectedtss = tssd
                elif sig['strand'] == '-' and tssd['TSSstrand'] == '-' and tssd['TSSrelP'] <= sig['S35start']:
                    middledist = (abs(tssd['TSSrelP'] - sig['S35start']) + abs(tssd['TSSrelP'] - sig['S35end'])) / 2
                    if 30 <= middledist <= 40:
                        expectedtss = tssd
            if sig['S10start'] != 'none':
                if sig['strand'] == '+' and tssd['TSSstrand'] == '+' and sig['S10start'] <= tssd['TSSrelP']:
                    middledist = (abs(tssd['TSSrelP'] - sig['S10start']) + abs(tssd['TSSrelP'] - sig['S10end'])) / 2
                    if 6 <= middledist <= 14:
                        expectedtss = tssd
                elif sig['strand'] == '-' and tssd['TSSstrand'] == '-' and tssd['TSSrelP'] <= sig['S10start']:
                    middledist = (abs(tssd['TSSrelP'] - sig['S10start']) + abs(tssd['TSSrelP'] - sig['S10end'])) / 2
                    if 6 <= middledist <= 14:
                        expectedtss = tssd
    return expectedtss


def buildcsvdict(methylist):
    errorlist = list()  # list stores corner cases / violated assumptions to be written to a log file
    mcsv = list()  # list stores the dictionaries with data on each base, each dictionary becomes a row in output csv
    for methyl in methylist:
        # extract the methylation data etc
        rowdict = dict(position=methyl['Start'], strand=methyl['Strand'], coverage=methyl['coverage'],
                       ipd_ratio=methyl['IPDRatio'], tmean=methyl['tMean'], modelPrediction=methyl['modelPrediction'],
                       Blow_Call=methyl['classification'], context=methyl['context'], base=methyl['base'],
                       BayesClass=methyl['BayesClass'], pdf_meth=methyl['pdf_meth'], pdf_unmeth=methyl['pdf_unmeth'],
                       RefLoci=methyl['RefLoci'], NearestCDS=methyl['NearestCDS'], NearestCDSft=methyl['NearestCDSft'],
                       NearestCDSrelpo=methyl['NearestCDSrelpo'])
        # extract MTase Motif Data
        rowdict['meth_Motif'] = 'none'
        rowdict['Mtase'] = 'none'
        if methyl['AllMTase']:  # value is either False or a list of motifs surrounding/overlapping the base
            for motif in methyl['AllMTase']:
                if motif['MotLap'] and motif['MotStrand'] == methyl['Strand'] \
                        and int(motif['ExpMeth']) == abs(motif['MotStart']) + 1:
                        # +1 because the methylated base is position 0 relative to MotStart
                    rowdict['meth_Motif'] = motif['MotType']
                    rowdict['Mtase'] = motif['MotName']
        # extract CDS data. record each CDS that overlaps the methylated base, only records 1st 3
        rowdict['dist_to_CDS1_start'] = 'none'
        rowdict['dist_to_CDS1_stop'] = 'none'
        rowdict['CDS1'] = 'none'
        rowdict['CDS1LT'] = 'none'
        rowdict['dist_to_CDS2_start'] = 'none'
        rowdict['dist_to_CDS2_stop'] = 'none'
        rowdict['CDS2'] = 'none'
        rowdict['CDS2LT'] = 'none'
        rowdict['dist_to_CDS3_start'] = 'none'
        rowdict['dist_to_CDS3_stop'] = 'none'
        rowdict['CDS3'] = 'none'
        rowdict['CDS3LT'] = 'none'
        if methyl['IntraCDS']:
            cds = methyl['AllCDS'][0]
            rowdict['dist_to_CDS1_start'] = cds['CDSrelstart']
            rowdict['dist_to_CDS1_stop'] = cds['CDSrelstop']
            rowdict['CDS1'] = cds['CDS']
            rowdict['CDS1LT'] = cds['CDSLT']
            if len(methyl['AllCDS']) > 1:
                cds = methyl['AllCDS'][1]
                rowdict['dist_to_CDS2_start'] = cds['CDSrelstart']
                rowdict['dist_to_CDS2_stop'] = cds['CDSrelstop']
                rowdict['CDS2'] = cds['CDS']
                rowdict['CDS2LT'] = cds['CDSLT']
            if len(methyl['AllCDS']) > 2:
                cds = methyl['AllCDS'][2]
                rowdict['dist_to_CDS3_start'] = cds['CDSrelstart']
                rowdict['dist_to_CDS3_stop'] = cds['CDSrelstop']
                rowdict['CDS3'] = cds['CDS']
                rowdict['CDS3LT'] = cds['CDSLT']
            if len(methyl['AllCDS']) > 3:
                errorlist.append('Over 3 CDS Overlapping Methylated Base: ' + str(methyl['Start']) + methyl['Strand'])
                cdsextralap = ''
                for cdsl in methyl['AllCDS']:
                    cdsextralap = cdsextralap + cdsl['CDS'] + ', '

        # extract SFBS data. Record SFBS motif that overlaps methyl. only records one match per SF Type (a, f, etc)
        truetss = False
        for sigtype in ['SigA', 'SigB', 'SigC', 'SigD', 'SigE', 'SigF', 'SigG', 'SigH', 'SigI', 'SigJ', 'SigK', 'SigL',
                        'SigM']:

            key10 = sigtype + '10Start'
            rowdict[key10] = methyl[sigtype]['S10start']
            newkey = sigtype + '10End'
            rowdict[newkey] = methyl[sigtype]['S10end']
            newkey = sigtype + 'ext'
            rowdict[newkey] = methyl[sigtype]['ext']
            newkey = sigtype + '10Full'
            rowdict[newkey] = methyl[sigtype]['S10full']
            key35 = sigtype + '35Start'
            rowdict[key35] = methyl[sigtype]['S35start']
            newkey = sigtype + '35End'
            rowdict[newkey] = methyl[sigtype]['S35end']
            newkey = sigtype + '35Full'
            rowdict[newkey] = methyl[sigtype]['S35full']
            newkey = sigtype + 'strand'
            rowdict[newkey] = methyl[sigtype]['strand']
            keyreal = sigtype + 'real'
            rowdict[keyreal] = False
            realsigtss = False
            if rowdict[key35] != 'none' or rowdict[key10] != 'none':
                realsigtss = truesig(methyl['AllTSS'], methyl[sigtype])  # will return false, or return with a TSS,
                # if any are in the expected position relative to the SFBS overlapping the methyl
            if realsigtss:
                rowdict[keyreal] = True
                truetss = realsigtss

        # extract TSS data. if a true SFBS overlaps the methylated base, will record the TSS of that SFBS
        if truetss is not False:
            rowdict['TSS_downstream_gene'] = truetss['GeneName']
            rowdict['TSS_downstream_geneLT'] = truetss['GeneLT']
            rowdict['TSS_internal_gene'] = truetss['IntraCDS']
            rowdict['TSS_internal_geneLT'] = truetss['IntraCDSLT']
            rowdict['dist_from_TSS'] = truetss['TSSrelP']
            rowdict['TSS_strand'] = truetss['TSSstrand']
            rowdict['TSS_condition'] = truetss['TSScondition']
            rowdict['TSS_type'] = truetss['TSStype']
        else:  # otherwise records nearest TSS, regardless of strand.  CHANGE to only accept TSS that m is upstream to
            rowdict['TSS_downstream_gene'] = 'none'
            rowdict['TSS_downstream_geneLT'] = 'none'
            rowdict['TSS_internal_gene'] = 'none'
            rowdict['TSS_internal_geneLT'] = 'none'
            rowdict['dist_from_TSS'] = 'none'
            rowdict['TSS_strand'] = 'none'
            rowdict['TSS_condition'] = 'none'
            rowdict['TSS_type'] = 'none'
            if methyl['AllTSS']:
                rowdict['dist_from_TSS'] = 200  # no TSS over 100 bp from methyl should be in methyl['AllTSS']
                for tss in methyl['AllTSS']:
                    if abs(tss['TSSrelP']) < rowdict['dist_from_TSS']:
                        rowdict['TSS_downstream_gene'] = tss['GeneName']
                        rowdict['TSS_downstream_geneLT'] = tss['GeneLT']
                        rowdict['TSS_internal_gene'] = tss['IntraCDS']
                        rowdict['TSS_internal_geneLT'] = tss['IntraCDSLT']
                        rowdict['dist_from_TSS'] = tss['TSSrelP']
                        rowdict['TSS_strand'] = tss['TSSstrand']
                        rowdict['TSS_condition'] = tss['TSScondition']
                        rowdict['TSS_type'] = tss['TSStype']
        # extract info on any overlapping G4 quadruplex motif
        rowdict['position_in_G4_motif'] = 'none'
        rowdict['G4quad_motif_type'] = 'none'
        if methyl['AllG4quad']:
            for g4 in methyl['AllG4quad']:
                if g4['MotLap']:
                    rowdict['position_in_G4_motif'] = abs(int(g4['MotStart']))
                    rowdict['G4quad_motif_type'] = g4['MotType']
        mcsv.append(rowdict)
    return mcsv


def main():
    # python tss-mtase-counter.py -p 1-0007_methylap.p -o 1-0007-sigmtaselap.csv
    parser = argparse.ArgumentParser(description='Counts Overlap Between Sigma Factor and Mtase Motifs Near TSS',
                                     epilog='Uses output pickle file of methylap.py')
    parser.add_argument('-p', '--pic', help='Filename for input', default='1-0007_methylap.p')
    parser.add_argument('-o', '--outfile', help='Filename for output', default='master-methylap.db')
    parser.add_argument('-e', '--elog', help='Filename for error logs', default='1-0007-cucmbler-errors.txt')
    parser.add_argument('-d', '--dst', help='Filename for DST data', default='/grp/valafar/metadata/dst.txt')
    parser.add_argument('-l', '--lin', help='Filename for Lineage data', default='/grp/valafar/metadata/lineage.txt')
    parser.add_argument('-i', '--iso', help='Isolate ID', default='1-0007')
    args = parser.parse_args()
    # read files
    with open(args.pic) as pfile:
        methylistin = pickle.load(pfile)

    # build list of dictionaries with data for tables in database
    outcsvdictlist = buildcsvdict(methylistin)

    # add to database (building database if it doesn't already exist)
    try:
        conn = sqlite3.connect(args.outfile)  # connects to database. SQLite makes one if it doesnt exist
        print(sqlite3.version)
    except Error as e:
        print('ERROR:')
        print(e)

    # extract the IsolateID (an integer primary key) of the isolate from the Metadata table of the database
    cur = conn.cursor()
    querycommand = 'SELECT * FROM Metadata WHERE Isolate = ' + '\'' + args.iso + '\''
    try:
        cur.execute(querycommand)
        rows = cur.fetchall()
        isolate_id = rows[0][0]  # first (and only row) matching the Isolate name, first column is IsolateID
    except Error as e:
        print('ERROR:')
        print(e)
        isolate_id = 1000
    conn.close()

    # now add a table
    # these sqlite command create tables and assign their column names
    tb_kinetics = """ CREATE TABLE IF NOT EXISTS KineticsProcessed (
                                        id integer PRIMARY KEY,
                                        position integer,
                                        base text NOT NULL,
                                        strand text NOT NULL,
                                        coverage integer,
                                        ipd_ratio real,
                                        tmean real,
                                        modelPrediction real,
                                        pdf_meth real,
                                        pdf_unmeth real,
                                        BayesClass text NOT NULL,
                                        Blow_Call text NOT NULL,
                                        meth_Motif text NOT NULL,
                                        Mtase text NOT NULL,
                                        context text NOT NULL,
                                        RefLoci text NOT NULL,
                                        position_in_G4_motif text NOT NULL,
                                        G4quad_motif_type text NOT NULL,
                                        isolate text NOT NULL,
                                        IsolateID integer NOT NULL,
                                        FOREIGN KEY (IsolateID) REFERENCES Metadata (IsolateID)
                                    ); """

    tb_cds = """CREATE TABLE IF NOT EXISTS CDS_Colocate (
                                    id integer PRIMARY KEY,
                                    dist_to_CDS1_start text NOT NULL,
                                    dist_to_CDS1_stop text NOT NULL,
                                    CDS1 text NOT NULL,
                                    CDS1LT text NOT NULL,
                                    dist_to_CDS2_start text NOT NULL,
                                    dist_to_CDS2_stop text NOT NULL,
                                    CDS2 text NOT NULL,
                                    CDS2LT text NOT NULL,
                                    dist_to_CDS3_start text NOT NULL,
                                    dist_to_CDS3_stop text NOT NULL,
                                    CDS3 text NOT NULL,
                                    CDS3LT text NOT NULL,
                                    RefLoci text NOT NULL,
                                    NearestCDS text NOT NULL,
                                    NearestCDSft text NOT NULL,
                                    NearestCDSrelpo text NOT NULL,
                                    KineticsProcessedID integer NOT NULL,
                                    FOREIGN KEY (KineticsProcessedID) REFERENCES KineticsProcessed (id)
                                );"""

    tb_tss = """CREATE TABLE IF NOT EXISTS TSS_Colocate (
                                        id integer PRIMARY KEY,
                                        RefLoci text NOT NULL,
                                        TSS_downstream_gene text NOT NULL,
                                        TSS_downstream_geneLT text NOT NULL,
                                        TSS_internal_gene text NOT NULL,
                                        TSS_internal_geneLT text NOT NULL,
                                        dist_from_TSS text NOT NULL,
                                        TSS_strand text NOT NULL,
                                        TSS_condition text NOT NULL,
                                        TSS_type text NOT NULL,
                                        SigA10Start text NOT NULL,
                                        SigA10End text NOT NULL,
                                        SigAext text NOT NULL,
                                        SigA10Full text NOT NULL,
                                        SigA35Start text NOT NULL,
                                        SigA35End text NOT NULL,
                                        SigA35Full text NOT NULL,
                                        SigAstrand text NOT NULL,
                                        SigAreal text NOT NULL,
                                        SigB10Start text NOT NULL,
                                        SigB10End text NOT NULL,
                                        SigBext text NOT NULL,
                                        SigB10Full text NOT NULL,
                                        SigB35Start text NOT NULL,
                                        SigB35End text NOT NULL,
                                        SigB35Full text NOT NULL,
                                        SigBstrand text NOT NULL,
                                        SigBreal text NOT NULL,
                                        SigC10Start text NOT NULL,
                                        SigC10End text NOT NULL,
                                        SigCext text NOT NULL,
                                        SigC10Full text NOT NULL,
                                        SigC35Start text NOT NULL,
                                        SigC35End text NOT NULL,
                                        SigC35Full text NOT NULL,
                                        SigCstrand text NOT NULL,
                                        SigCreal text NOT NULL,
                                        SigD10Start text NOT NULL,
                                        SigD10End text NOT NULL,
                                        SigDext text NOT NULL,
                                        SigD10Full text NOT NULL,
                                        SigD35Start text NOT NULL,
                                        SigD35End text NOT NULL,
                                        SigD35Full text NOT NULL,
                                        SigDstrand text NOT NULL,
                                        SigDreal text NOT NULL,
                                        SigE10Start text NOT NULL,
                                        SigE10End text NOT NULL,
                                        SigEext text NOT NULL,
                                        SigE10Full text NOT NULL,
                                        SigE35Start text NOT NULL,
                                        SigE35End text NOT NULL,
                                        SigE35Full text NOT NULL,
                                        SigEstrand text NOT NULL,
                                        SigEreal text NOT NULL,
                                        SigF10Start text NOT NULL,
                                        SigF10End text NOT NULL,
                                        SigFext text NOT NULL,
                                        SigF10Full text NOT NULL,
                                        SigF35Start text NOT NULL,
                                        SigF35End text NOT NULL,
                                        SigF35Full text NOT NULL,
                                        SigFstrand text NOT NULL,
                                        SigFreal text NOT NULL,
                                        SigG10Start text NOT NULL,
                                        SigG10End text NOT NULL,
                                        SigGext text NOT NULL,
                                        SigG10Full text NOT NULL,
                                        SigG35Start text NOT NULL,
                                        SigG35End text NOT NULL,
                                        SigG35Full text NOT NULL,
                                        SigGstrand text NOT NULL,
                                        SigGreal text NOT NULL,
                                        SigH10Start text NOT NULL,
                                        SigH10End text NOT NULL,
                                        SigHext text NOT NULL,
                                        SigH10Full text NOT NULL,
                                        SigH35Start text NOT NULL,
                                        SigH35End text NOT NULL,
                                        SigH35Full text NOT NULL,
                                        SigHstrand text NOT NULL,
                                        SigHreal text NOT NULL,
                                        SigI10Start text NOT NULL,
                                        SigI10End text NOT NULL,
                                        SigIext text NOT NULL,
                                        SigI10Full text NOT NULL,
                                        SigI35Start text NOT NULL,
                                        SigI35End text NOT NULL,
                                        SigI35Full text NOT NULL,
                                        SigIstrand text NOT NULL,
                                        SigIreal text NOT NULL,
                                        SigJ10Start text NOT NULL,
                                        SigJ10End text NOT NULL,
                                        SigJext text NOT NULL,
                                        SigJ10Full text NOT NULL,
                                        SigJ35Start text NOT NULL,
                                        SigJ35End text NOT NULL,
                                        SigJ35Full text NOT NULL,
                                        SigJstrand text NOT NULL,
                                        SigJreal text NOT NULL,
                                        SigK10Start text NOT NULL,
                                        SigK10End text NOT NULL,
                                        SigKext text NOT NULL,
                                        SigK10Full text NOT NULL,
                                        SigK35Start text NOT NULL,
                                        SigK35End text NOT NULL,
                                        SigK35Full text NOT NULL,
                                        SigKstrand text NOT NULL,
                                        SigKreal text NOT NULL,
                                        SigL10Start text NOT NULL,
                                        SigL10End text NOT NULL,
                                        SigLext text NOT NULL,
                                        SigL10Full text NOT NULL,
                                        SigL35Start text NOT NULL,
                                        SigL35End text NOT NULL,
                                        SigL35Full text NOT NULL,
                                        SigLstrand text NOT NULL,
                                        SigLreal text NOT NULL,
                                        SigM10Start text NOT NULL,
                                        SigM10End text NOT NULL,
                                        SigMext text NOT NULL,
                                        SigM10Full text NOT NULL,
                                        SigM35Start text NOT NULL,
                                        SigM35End text NOT NULL,
                                        SigM35Full text NOT NULL,
                                        SigMstrand text NOT NULL,
                                        SigMreal text NOT NULL,
                                        KineticsProcessedID integer NOT NULL,
                                        FOREIGN KEY (KineticsProcessedID) REFERENCES KineticsProcessed (id)
                                    );"""
    try:
        conn = sqlite3.connect(args.outfile)
        cur = conn.cursor()
        cur.execute(tb_kinetics)
        cur.execute(tb_cds)
        cur.execute(tb_tss)
        print('tables made')
    except Error as e:
        print('ERROR:')
        print(e)

    conn.close()

    # now add data to the table
    conn = sqlite3.connect(args.outfile)
    with conn:
        cur = conn.cursor()
        kineticsinsert = ''' INSERT INTO KineticsProcessed(position,base,strand,coverage,ipd_ratio,tmean,modelPrediction,
                          pdf_meth,pdf_unmeth,BayesClass,Blow_Call,meth_Motif,Mtase,context,RefLoci,
                          position_in_G4_motif,G4quad_motif_type,isolate,IsolateID)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) '''

        cdsinsert = ''' INSERT INTO CDS_Colocate(dist_to_CDS1_start,dist_to_CDS1_stop,CDS1,CDS1LT,dist_to_CDS2_start,
                          dist_to_CDS2_stop,CDS2,CDS2LT,dist_to_CDS3_start,dist_to_CDS3_stop,CDS3,CDS3LT,RefLoci,
                          NearestCDS,NearestCDSft,NearestCDSrelpo,KineticsProcessedID)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) '''

        tssinsert = ''' INSERT INTO TSS_Colocate(RefLoci,TSS_downstream_gene,TSS_downstream_geneLT,TSS_internal_gene,
                          TSS_internal_geneLT,dist_from_TSS,TSS_strand,TSS_condition,
                          TSS_type,SigA10Start,SigA10End,SigAext,SigA10Full,SigA35Start,SigA35End,SigA35Full,SigAstrand,
                          SigAreal,SigB10Start,SigB10End,SigBext,SigB10Full,SigB35Start,SigB35End,SigB35Full,SigBstrand,
                          SigBreal,SigC10Start,SigC10End,SigCext,SigC10Full,SigC35Start,SigC35End,SigC35Full,SigCstrand,
                          SigCreal,SigD10Start,SigD10End,SigDext,SigD10Full,SigD35Start,SigD35End,SigD35Full,SigDstrand,
                          SigDreal,SigE10Start,SigE10End,SigEext,SigE10Full,SigE35Start,SigE35End,SigE35Full,SigEstrand,
                          SigEreal,SigF10Start,SigF10End,SigFext,SigF10Full,SigF35Start,SigF35End,SigF35Full,SigFstrand,
                          SigFreal,SigG10Start,SigG10End,SigGext,SigG10Full,SigG35Start,SigG35End,SigG35Full,SigGstrand,
                          SigGreal,SigH10Start,SigH10End,SigHext,SigH10Full,SigH35Start,SigH35End,SigH35Full,SigHstrand,
                          SigHreal,SigI10Start,SigI10End,SigIext,SigI10Full,SigI35Start,SigI35End,SigI35Full,SigIstrand,
                          SigIreal,SigJ10Start,SigJ10End,SigJext,SigJ10Full,SigJ35Start,SigJ35End,SigJ35Full,SigJstrand,
                          SigJreal,SigK10Start,SigK10End,SigKext,SigK10Full,SigK35Start,SigK35End,SigK35Full,SigKstrand,
                          SigKreal,SigL10Start,SigL10End,SigLext,SigL10Full,SigL35Start,SigL35End,SigL35Full,SigLstrand,
                          SigLreal,SigM10Start,SigM10End,SigMext,SigM10Full,SigM35Start,SigM35End,SigM35Full,SigMstrand,
                          SigMreal,KineticsProcessedID)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                                  ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                                  ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                                  ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,
                                  ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) '''

        for methyldict in outcsvdictlist:
            kineticsrow = (methyldict['position'], methyldict['base'], methyldict['strand'], methyldict['coverage'],
                           methyldict['ipd_ratio'], methyldict['tmean'], methyldict['modelPrediction'],
                           methyldict['pdf_meth'], methyldict['pdf_unmeth'], methyldict['BayesClass'],
                           methyldict['Blow_Call'], methyldict['meth_Motif'], methyldict['Mtase'],
                           methyldict['context'], methyldict['RefLoci'], methyldict['position_in_G4_motif'],
                           methyldict['G4quad_motif_type'], args.iso, isolate_id)
            cur.execute(kineticsinsert, kineticsrow)
            kinetics_id = cur.lastrowid

            cdsrow = (methyldict['dist_to_CDS1_start'], methyldict['dist_to_CDS1_stop'], methyldict['CDS1'],
                      methyldict['CDS1LT'], methyldict['dist_to_CDS2_start'], methyldict['dist_to_CDS2_stop'],
                      methyldict['CDS2'], methyldict['CDS2LT'], methyldict['dist_to_CDS3_start'],
                      methyldict['dist_to_CDS3_stop'], methyldict['CDS3'], methyldict['CDS3LT'], methyldict['RefLoci'],
                      methyldict['NearestCDS'], methyldict['NearestCDSft'], methyldict['NearestCDSrelpo'], kinetics_id)
            cur.execute(cdsinsert, cdsrow)

            tssrow = (methyldict['RefLoci'], methyldict['TSS_downstream_gene'], methyldict['TSS_downstream_geneLT'],
                      methyldict['TSS_internal_gene'], methyldict['TSS_internal_geneLT'], methyldict['dist_from_TSS'],
                      methyldict['TSS_strand'], methyldict['TSS_condition'], methyldict['TSS_type'],
                      methyldict['SigA10Start'], methyldict['SigA10End'], methyldict['SigAext'],
                      methyldict['SigA10Full'], methyldict['SigA35Start'], methyldict['SigA35End'],
                      methyldict['SigA35Full'], methyldict['SigAstrand'], methyldict['SigAreal'],
                      methyldict['SigB10Start'], methyldict['SigB10End'], methyldict['SigBext'],
                      methyldict['SigB10Full'], methyldict['SigB35Start'], methyldict['SigB35End'],
                      methyldict['SigB35Full'], methyldict['SigBstrand'], methyldict['SigBreal'],
                      methyldict['SigC10Start'], methyldict['SigC10End'], methyldict['SigCext'],
                      methyldict['SigC10Full'], methyldict['SigC35Start'], methyldict['SigC35End'],
                      methyldict['SigC35Full'], methyldict['SigCstrand'], methyldict['SigCreal'],
                      methyldict['SigD10Start'], methyldict['SigD10End'], methyldict['SigDext'],
                      methyldict['SigD10Full'], methyldict['SigD35Start'], methyldict['SigD35End'],
                      methyldict['SigD35Full'], methyldict['SigDstrand'], methyldict['SigDreal'],
                      methyldict['SigE10Start'], methyldict['SigE10End'], methyldict['SigEext'],
                      methyldict['SigE10Full'], methyldict['SigE35Start'], methyldict['SigE35End'],
                      methyldict['SigE35Full'], methyldict['SigEstrand'], methyldict['SigEreal'],
                      methyldict['SigF10Start'], methyldict['SigF10End'], methyldict['SigFext'],
                      methyldict['SigF10Full'], methyldict['SigF35Start'], methyldict['SigF35End'],
                      methyldict['SigF35Full'], methyldict['SigFstrand'], methyldict['SigFreal'],
                      methyldict['SigG10Start'], methyldict['SigG10End'], methyldict['SigGext'],
                      methyldict['SigG10Full'], methyldict['SigG35Start'], methyldict['SigG35End'],
                      methyldict['SigG35Full'], methyldict['SigGstrand'], methyldict['SigGreal'],
                      methyldict['SigH10Start'], methyldict['SigH10End'], methyldict['SigHext'],
                      methyldict['SigH10Full'], methyldict['SigH35Start'], methyldict['SigH35End'],
                      methyldict['SigH35Full'], methyldict['SigHstrand'], methyldict['SigHreal'],
                      methyldict['SigI10Start'], methyldict['SigI10End'], methyldict['SigIext'],
                      methyldict['SigI10Full'], methyldict['SigI35Start'], methyldict['SigI35End'],
                      methyldict['SigI35Full'], methyldict['SigIstrand'], methyldict['SigIreal'],
                      methyldict['SigJ10Start'], methyldict['SigJ10End'], methyldict['SigJext'],
                      methyldict['SigJ10Full'], methyldict['SigJ35Start'], methyldict['SigJ35End'],
                      methyldict['SigJ35Full'], methyldict['SigJstrand'], methyldict['SigJreal'],
                      methyldict['SigK10Start'], methyldict['SigK10End'], methyldict['SigKext'],
                      methyldict['SigK10Full'], methyldict['SigK35Start'], methyldict['SigK35End'],
                      methyldict['SigK35Full'], methyldict['SigKstrand'], methyldict['SigKreal'],
                      methyldict['SigL10Start'], methyldict['SigL10End'], methyldict['SigLext'],
                      methyldict['SigL10Full'], methyldict['SigL35Start'], methyldict['SigL35End'],
                      methyldict['SigL35Full'], methyldict['SigLstrand'], methyldict['SigLreal'],
                      methyldict['SigM10Start'], methyldict['SigM10End'], methyldict['SigMext'],
                      methyldict['SigM10Full'], methyldict['SigM35Start'], methyldict['SigM35End'],
                      methyldict['SigM35Full'], methyldict['SigMstrand'], methyldict['SigMreal'],
                      kinetics_id)
            cur.execute(tssinsert, tssrow)
    # and close the connection to the database before finishing
    conn.close()

if __name__ == '__main__':
    main()
