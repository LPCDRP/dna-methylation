#Makefile to organize creation of mutlifasta file from methylation data
#call like this:
#make -f ../src/findmethyls.mk ISOLATE=1-0007
#for all isolates call like this:
#for isolate in $(cat ../targets/isolates.txt); do make -f ../src/findmethyls.mk ISOLATE=$isolate; done
#.ONE_SHELL:


#Variables
ISOLATE ?= 1-0007
database = master-methylap.db
source_dir = $(GROUPHOME)/data/methylation/
source_csv = $(GROUPHOME)/data/depot/assembly/$(ISOLATE)/methylation/data/modifications.csv.gz
sourcemotifs = $(GROUPHOME)/data/depot/assembly/$(ISOLATE)/methylation/data/motifs.gff.gz
sourcegenbank = $(GROUPHOME)/data/annotation/$(ISOLATE).gbk
sourcebstats = $(GROUPHOME)/data/methylation/base-stats-all-isolates.tsv
source_fasta = /grp/valafar/data/genomes/$(ISOLATE).fasta
nproc ?= 6

#Recipies
all : $(ISOLATE)/$(ISOLATE)_methylap.p $(database)
	../src/cucumbler.py -p $(ISOLATE)/$(ISOLATE)_methylap.p -o $(database) -i $(ISOLATE)

$(ISOLATE)/$(ISOLATE)_methylap.p : $(ISOLATE)/$(ISOLATE)-methyls.csv
	qsub -cwd -N methylap -j yes -b yes -o findmeth.log -pe smp $(nproc) -sync yes timeout 7200s ../src/methylap.py -o $@ -m $^ -e $(sourcegenbank) -n $(nproc) -s ../targets/sigmotifs.txt -t ../targets/mtase-motifs.txt -g ../targets/g4quad-motifs.txt 

$(ISOLATE)/$(ISOLATE)-methyls.csv : $(ISOLATE)/$(ISOLATE)motifs.csv $(source_csv) $(source_fasta) $(sourcebstats)
	Rscript ../src/findmethyls.R $^ $(ISOLATE) $@

$(ISOLATE)/$(ISOLATE)motifs.csv : $(sourcemotifs) $(ISOLATE)
	../src/methylgffpreproc.py -i $(word 1,$^) -o $@

$(ISOLATE) : 
	mkdir -p $(ISOLATE)

$(database): $(source_dir)/mt-motifs.tsv $(source_dir)/mtase_variants.tsv $(source_dir)/mt-mutation-activity.txt
	../src/db-metadata.py -d $(database) -i $(source_dir)/isolates.txt
