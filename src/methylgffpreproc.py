#!/usr/bin/env python2.7
import gzip
import csv
import argparse


def parseline(line):
    cleanline = line.strip('\n')  # removes newline char from string
    entry = cleanline.split('\t')  # elements of line deliminated by tab
    infocol = entry[-1].split(';')  # last element of line further deliminated by ;
    # store data from desired columns in dictionary
    gffdict = {'Type': entry[2], 'Start': entry[3], 'Score': entry[5], 'Strand': entry[6]}
    # convert data from info column into its own dictionary
    infodict = {}
    for note in infocol:
        keyval = note.split('=')  # each key value pair in info column of gff is separated by =
        nkey = keyval[0]
        infodict[nkey] = keyval[1]
    # transfer desired data from infodict to gffdict, adding placeholders for keys not present in this line
    for dkey in ['coverage', 'IPDRatio', 'context', 'motif', 'fracLow', 'fracUp', 'frac']:
        if dkey in infodict:
            gffdict[dkey] = infodict[dkey]
        else:
            gffdict[dkey] = '.'
    return gffdict


def main():
    # call like this:
    # python methylgffpreproc.py -i $GROUPHOME/data/depot/assembly/1-0061/methylation/data/motifs.gff.gz -o 1-0061motifs.csv
    parser = argparse.ArgumentParser(description='preprocesses motifs.gff.gz files for findmethyls.R',
                                     epilog='Takes in a motifs.gff.fz file and outputs a csv for findmethyls.R')
    parser.add_argument('-o', '--outfile', help='Filename for output')
    parser.add_argument('-i', '--infile', help='Filename for input')
    args = parser.parse_args()

    listdicts = []
    with gzip.open(args.infile) as infile:
        for row in infile:
            if row[0] != '#':  # skipping the lines that start with #
                rowdict = parseline(row)        # stores data from line in a dictionary
                listdicts.append(rowdict)  # store dictionaries from each line in a list

    # write out
    with open(args.outfile, 'w') as csvfile:
        fieldnames1 = ['Type', 'Start', 'Strand', 'Score', 'coverage', 'IPDRatio', 'context', 'motif', 'fracLow',
                       'fracUp', 'frac']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames1, delimiter='\t')
        writer.writeheader()
        for row in listdicts:
            writer.writerow(row)


if __name__ == '__main__':
    main()

