#!/usr/bin/env python2.7
import os
import os.path

fimo_directory = '/home/dconkleg/workspace/methylation/dna-methylation/fimo/'
transplant_directory = '/home/smitchel/methylation/mtase-variants/mtase_fimo/'


def main(fimo_dir):
    for mtase_dir in os.listdir(fimo_dir):
        mtase_master_list = []
        if 'loci' in mtase_dir:
            mtase_dir = fimo_dir + mtase_dir
            for tf_dir in os.listdir(mtase_dir):
                tf_dir = mtase_dir + '/' + tf_dir
                for fimo_files in os.listdir(tf_dir):
                    if fimo_files == 'fimo.txt':
                        fimo_file = tf_dir + '/' + fimo_files
                        for line in file(fimo_file):
                            line = line.strip()
                            if line.startswith('#'):
                                continue
                            else:
                                line = line.split('\t')
                                sequence_name = line[2]
                                start = line[3]
                                stop = line[4]
                                strand = line[5]
                                score = line[6]
                                pvalue = line[7]
                                qvalue = line[8]
                                matched_sequence = line[9]
                                tf_matched = tf_dir.split('/')[8].split('_')[0]
                                pal_status = tf_dir.split('/')[8].split('_')[1]
                                meth_status = mtase_dir.split('/')[7].split('-')[0]
                                mtase_id = tf_dir.split('/')[7].split('-')[2]
                                mtase_master_list.append([sequence_name, ', ', start, ', ', stop, ', ', strand, ', ',
                                                          score, ', ', pvalue, ', ', qvalue, ', ', matched_sequence, ', ',
                                                          tf_matched, ', ', pal_status, ', ', meth_status, ', ', mtase_id])
            mtase_write_dir = mtase_dir.split('/')[7]
            if not os.path.exists(transplant_directory + mtase_write_dir):
                os.makedirs(transplant_directory + mtase_write_dir)
            with open(transplant_directory + mtase_write_dir + '/' + 'fimo.txt', 'wb') as f1:
                header = '# sequence_name, start, stop, strand, score, p - value, q - value, matched_sequence, ' \
                         'transcription_factor, palindrome_status, methylation_status, MTase'
                f1.writelines(header)
                f1.writelines('\n')
                for line in mtase_master_list:
                    f1.writelines(line)
                    f1.writelines('\n')


if __name__ == '__main__':
    main(fimo_dir=fimo_directory)
