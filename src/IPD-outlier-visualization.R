##### Load in packages and set themes and variables #####
library(ggplot2)
library(ggthemes)
library(dplyr)
library(stringr) # For manipulating strings
library(data.table)

##### Aesthetics #####
lava <- c("#000000","#0099CC", "#FF6666","#0072B2", "#CC6666","#332288", "#999999","#6699CC", "#88CCEE", "#44AA99","#D55E00")
volcano <- scale_colour_manual(values=lava)
theme_set(theme_minimal(base_size = 18, base_family = "Helvetica")) # this can be changed based on aesthetic preference. See ggthemes() documentation for examples of different styles

##### Read Input #####
methlap <- fread("/grp/valafar/data/methylation/master-methylap-polished.csv") # Main data


methlap$threshold <- 0
methlap$threshold[methlap$log_ipd <= 1.5] <- 1
methlap$threshold <- as.factor(methlap$threshold)

base <- ggplot(data=methlap[methlap$lineage=="East-Asian" & Mtase == "hsdM"])
IPD_outlier <- base + geom_point(aes(x=position,y=log_ipd,color=threshold), alpha=0.5)
IPD_outlier + facet_wrap(~isolate)
