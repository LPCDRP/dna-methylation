#!/usr/bin/env python2.7
from Bio import SeqIO
import re
import argparse
import os
import csv
import pickle
from multiprocessing import Pool
from functools import partial
from tssearch import findtss

GROUPHOME = os.environ['GROUPHOME']


def parsemotif(motfile, delim):
    motlist = []  # initialize list to store motifs as dictionaries
    with open(motfile) as sfile:
        for line in sfile:
            cleanline = line.strip('\n')
            entry = cleanline.split(delim)
            motdict = dict(motif=entry[0], pattern=entry[1], name=entry[2], expectm=entry[3])
            motlist.append(motdict)
    return motlist


def parsesig(sigfile):
    siglist = []  # initialize list to store sigma motifs as dictionaries
    with open(sigfile) as sfile:
        next(sfile)  # skipping the first line
        for line in sfile:
            cleanline = line.strip('\n')
            entry = cleanline.split('\t')
            motdict = dict(sigtype=entry[0], tenext=entry[1], ten=entry[2],  # re patterns stored in dictionary
                           thirtyfive=entry[3], distmin=int(entry[4]), distmax=int(entry[5]))
            siglist.append(motdict)
    return siglist


def rematchlist(motpattern, pstrandsearchbp, nstrandsearchbp, regionsize, motkey, motname, expmeth):
    # searches positive and negative strand for matches to motif
    normpile = re.compile(motpattern)   # compiles re pattern
    lookahv = '(?=' + motpattern + ')'  # adds a look-ahead to the pattern so it won't consume the string
    #  as it finds matches. allows it to find overlapping matches
    lapile = re.compile(lookahv)
    allmots = []  # initialize list to store dictionaries with data of each match to the motif pattern

    # search the positive strand
    for lamatch in lapile.finditer(pstrandsearchbp):
        motmatch = dict(MotType=motkey, MotStrand='+', MotName=motname, ExpMeth=expmeth)
        # dictionary to store the matching motif
        motmatch['MotStart'] = lamatch.start() - regionsize  # start position of motif relative to methylated base
        # the match object start is relative to the beginning of the 300 bp string,
        # the methyl is in the middle
        normmatch = normpile.match(pstrandsearchbp[lamatch.start():])  # to get the end of the motif, need a
        # match object found using the non lookahead pattern so it consumes the string, but searching a
        # substring starting at the start of the motif
        motmatch['MotEnd'] = normmatch.end() + lamatch.start() - regionsize - 1
        motmatch['MotSeq'] = normmatch.group()
        if motmatch['MotStart'] <= 0 <= motmatch['MotEnd']:
            motmatch['MotLap'] = True
        else:
            motmatch['MotLap'] = False
        allmots.append(motmatch)

    # search the negative strand
    for lamatch in lapile.finditer(nstrandsearchbp):
        motmatch = dict(MotType=motkey, MotStrand='-', MotName=motname, ExpMeth=expmeth)
        motmatch['MotStart'] = regionsize - 1 - lamatch.start()
        # the beginning of this string is the end of the sequence on the positive strand
        # the methyl is in the middle of this 300 bp sequence
        normmatch = normpile.match(nstrandsearchbp[lamatch.start():])
        motmatch['MotEnd'] = regionsize - (normmatch.end() + lamatch.start())
        motmatch['MotSeq'] = normmatch.group()
        if motmatch['MotEnd'] <= 0 <= motmatch['MotStart']:
            motmatch['MotLap'] = True
        else:
            motmatch['MotLap'] = False
        allmots.append(motmatch)
    return allmots


def findsfbs(methyl, siglist, genome, genomesize, regionsize):
    # get the sequence within 2*regionsize bp (so 300 bp) centered around the methylated base
    mstart = int(methyl['Start']) - 1  # convert methylated position to biopython sequence indexing for splicing
    if mstart < regionsize:   # accounting for origin
        search1 = genome[(genomesize + mstart - regionsize):genomesize]
        search2 = genome[0:(mstart + regionsize)]
        searchseq = search1 + search2
    elif mstart > (genomesize - regionsize):
        search1 = genome[(mstart - regionsize):genomesize]
        search2 = genome[0:(mstart - genomesize + regionsize)]
        searchseq = search1 + search2
    else:  # typical case
        searchseq = genome[(mstart - regionsize):(mstart + regionsize)]
    pstrandsearchbp = str(searchseq)  # convert to string for regular expression searching
    nstrandsearchbp = str(searchseq.reverse_complement())  # also grab the negative strand

    # search for matches to re patterns of each Sigma factor type in the context strings
    for sig in siglist:
        # build a separate dictionary for each Sigma Type (for each methylated base)
        sigmatchdict = dict(S10start='none', S10end='none', ext=False, S10full=False,
                            S35start='none', S35end='none', S35full=False, strand='none')   # default values
        # look for pattern matches to -10ext, -10, and -35
        s10extmatches = rematchlist(motpattern=sig['tenext'], pstrandsearchbp=pstrandsearchbp,
                                    nstrandsearchbp=nstrandsearchbp, regionsize=regionsize,
                                    motkey='tenext', motname='sig', expmeth='none')
        s10matches = rematchlist(motpattern=sig['ten'], pstrandsearchbp=pstrandsearchbp,
                                 nstrandsearchbp=nstrandsearchbp, regionsize=regionsize,
                                 motkey='ten', motname='sig', expmeth='none')
        s35matches = rematchlist(motpattern=sig['thirtyfive'], pstrandsearchbp=pstrandsearchbp,
                                 nstrandsearchbp=nstrandsearchbp, regionsize=regionsize,
                                 motkey='thirtyfive', motname='sig', expmeth='none')
        # choose any -10ext that overlaps the methylated base
        if s10extmatches:  # false if empty
            for match in s10extmatches:
                if match['MotLap'] and sigmatchdict['S10start'] == 'none':  # will populate S10 if nothings there
                    sigmatchdict['S10start'] = match['MotStart']
                    sigmatchdict['S10end'] = match['MotEnd']
                    sigmatchdict['strand'] = match['MotStrand']
                    sigmatchdict['ext'] = True
                if match['MotLap'] and not sigmatchdict['S10full'] and s35matches:
                    for partnermatch in s35matches:
                        if match['MotStrand'] == partnermatch['MotStrand'] and \
                                ((sig['distmin'] - 3) <= abs(match['MotEnd'] - partnermatch[
                                    'MotStart']) <= (sig['distmax'] - 3)):
                            sigmatchdict['S10full'] = True
                            # prefers a S10 that was the right distance from a S35
                            sigmatchdict['S10start'] = match['MotStart']
                            sigmatchdict['S10end'] = match['MotEnd']
                            sigmatchdict['strand'] = match['MotStrand']
                            sigmatchdict['ext'] = True

        # if no overlapping S10ext, looks for overlapping S10
        if sigmatchdict['S10start'] == 'none' and s10matches:
            for match in s10matches:
                if match['MotLap'] and sigmatchdict['S10start'] == 'none':  # will populate S10 if nothings there
                    sigmatchdict['S10start'] = match['MotStart']
                    sigmatchdict['S10end'] = match['MotEnd']
                    sigmatchdict['strand'] = match['MotStrand']
                if match['MotLap'] and not sigmatchdict['S10full'] and s35matches:
                    for partnermatch in s35matches:
                        if match['MotStrand'] == partnermatch['MotStrand'] and \
                                (sig['distmin'] <= abs(match['MotStart'] - partnermatch['MotEnd']) <= sig['distmax']):
                            sigmatchdict['S10full'] = True
                             # prefers a S10 that was the right distance from a S35
                            sigmatchdict['S10start'] = match['MotStart']
                            sigmatchdict['S10end'] = match['MotEnd']
                            sigmatchdict['strand'] = match['MotStrand']

        # looks for S35 motifs overlapping the methylated base
        if s35matches:
            for match in s35matches:
                if match['MotLap'] and sigmatchdict['S35start'] == 'none':  # will populate S35 if nothings there
                    sigmatchdict['S35start'] = match['MotStart']
                    sigmatchdict['S35end'] = match['MotEnd']
                    sigmatchdict['strand'] = match['MotStrand']
                if match['MotLap'] and sigmatchdict['S10start'] == 'none' and s10matches:  # looks for nearby S10 otherwise
                    for partnermatch in s10matches:
                        if match['MotStrand'] == partnermatch['MotStrand'] and \
                                (sig['distmin'] <= abs(partnermatch['MotEnd'] - match['MotStart']) <= sig['distmax']):
                            sigmatchdict['S35full'] = True
                            # prefers a S35 that was the right distance from a S10
                            sigmatchdict['S35start'] = match['MotStart']
                            sigmatchdict['S35end'] = match['MotEnd']
                            sigmatchdict['strand'] = match['MotStrand']

        methylkey = sig['sigtype']
        methyl[methylkey] = sigmatchdict  # add the sigma data to the methyl, keyed by the sigma type (a, f, etc.)
    return methyl


def findmotifs(methyl, motlist, matchlistkey, genome, genomesize, regionsize):
    # get the sequence within 2*regionsize bp (so 300 bp) centered around the methylated base
    mstart = int(methyl['Start']) - 1  # convert methylated position to biopython sequence indexing for splicing
    if mstart < regionsize:
        search1 = genome[(genomesize + mstart - regionsize):genomesize]
        search2 = genome[0:(mstart + regionsize)]
        searchseq = search1 + search2
    elif mstart > (genomesize - regionsize):
        search1 = genome[(mstart - regionsize):genomesize]
        search2 = genome[0:(mstart - genomesize + regionsize)]
        searchseq = search1 + search2
    else:
        searchseq = genome[(mstart - regionsize):(mstart + regionsize)]
    pstrandsearchbp = str(searchseq)  # convert to string for regular expression searching
    nstrandsearchbp = str(searchseq.reverse_complement())  # also grab the negative strand

    # find all matches to motifs within the search sequence
    allmots = []
    for motif in motlist:
        motkeymatches = rematchlist(motpattern=motif['pattern'], pstrandsearchbp=pstrandsearchbp,
                                    nstrandsearchbp=nstrandsearchbp, regionsize=regionsize,
                                    motkey=motif['motif'], motname=motif['name'], expmeth=motif['expectm'])
        allmots += motkeymatches
    if allmots:   # checks that allmots list is nonempty, aka that there are nearby motifs for this methyl
        methyl[matchlistkey] = allmots
    else:
        methyl[matchlistkey] = False  # default false
    return methyl


def intracds(methyl, rattembl, genomesize):
    mstart = int(methyl['Start'])   # convert methylated position to biopython sequence indexing for comparing
    intracdslist = []  # stores any cds the methyl falls inside, either strand. cds info stored as dictionaries
    for feat in rattembl.features:
        if feat.type == 'CDS' and mstart in feat:  # uses biopython built in method to determine if position is in feat
            try:
                cds = dict(CDS=feat.qualifiers['gene'][0], CDSLT=feat.qualifiers['locus_tag'][0])
            except KeyError:
                cds = dict(CDS=feat.qualifiers['locus_tag'][0], CDSLT=feat.qualifiers['locus_tag'][0])
            if feat.location.strand == 1:
                cds['CDSstrand'] = '+'
                cds['CDSstart'] = feat.location.start + 1
                cds['CDSend'] = feat.location.end + 0
                cds['CDSrelstart'] = cds['CDSstart'] - mstart
                cds['CDSrelstop'] = cds['CDSend'] - mstart
            else:
                cds['CDSstrand'] = '-'
                cds['CDSstart'] = feat.location.end + 0
                cds['CDSend'] = feat.location.start + 1
                cds['CDSrelstart'] = cds['CDSstart'] - mstart
                cds['CDSrelstop'] = cds['CDSend'] - mstart
            intracdslist.append(cds)
    if intracdslist:   # checks that intracdslist list is nonempty, aka that the methyl is within at least one cds
        methyl['AllCDS'] = intracdslist
        methyl['IntraCDS'] = True
    else:
        methyl['IntraCDS'] = False  # default false
    return methyl


def nearestcds(methyl, rattembl, genomesize):
    mstart = int(methyl['Start']) - 1  # convert methylated position to biopython sequence indexing for comparing
    nearestdist = genomesize
    methyl['NearestCDS'] = 'none'
    methyl['NearestCDSft'] = 'none'
    methyl['NearestCDSrelpo'] = 'none'
    methyl['RefLoci'] = 'none'
    for feat in rattembl.features:
        if feat.type == 'CDS':
            cdsdist = abs(mstart - int(feat.location.start))  # check if each feature's start is closest to mstart
            if cdsdist < nearestdist:
                nearestdist = cdsdist
                try:
                    methyl['NearestCDS'] = feat.qualifiers['gene'][0]
                except KeyError:
                    methyl['NearestCDS'] = feat.qualifiers['locus_tag'][0]
                if feat.location.strand == 1:
                    methyl['NearestCDSft'] = 'Start'
                    methyl['NearestCDSrelpo'] = mstart - int(feat.location.start)
                else:
                    methyl['NearestCDSft'] = 'Stop'  # location.start is CDS Stop for - strand genes
                    methyl['NearestCDSrelpo'] = int(feat.location.start) - mstart

            cdsdist = abs(mstart - int(feat.location.end))
            if cdsdist < nearestdist:
                nearestdist = cdsdist
                try:
                    methyl['NearestCDS'] = feat.qualifiers['gene'][0]
                except KeyError:
                    methyl['NearestCDS'] = feat.qualifiers['locus_tag'][0]
                if feat.location.strand == 1:
                    methyl['NearestCDSft'] = 'Stop'
                    methyl['NearestCDSrelpo'] = mstart - int(feat.location.end)
                else:
                    methyl['NearestCDSft'] = 'Start'
                    methyl['NearestCDSrelpo'] = int(feat.location.end) - mstart

            cdsdist = genomesize - abs(mstart - int(feat.location.start))  # corner case: circular genome
            if cdsdist < nearestdist:
                nearestdist = cdsdist
                try:
                    methyl['NearestCDS'] = feat.qualifiers['gene'][0]
                except KeyError:
                    methyl['NearestCDS'] = feat.qualifiers['locus_tag'][0]
                if mstart > int(feat.location.start):
                    methyl['NearestCDSrelpo'] = -1 * cdsdist
                else:
                    methyl['NearestCDSrelpo'] = cdsdist
                if feat.location.strand == 1:
                    methyl['NearestCDSft'] = 'Start'
                else:
                    methyl['NearestCDSft'] = 'Stop'
                    methyl['NearestCDSrelpo'] = -1 * methyl['NearestCDSrelpo']

            cdsdist = genomesize - abs(mstart - int(feat.location.end))
            if cdsdist < nearestdist:
                nearestdist = cdsdist
                try:
                    methyl['NearestCDS'] = feat.qualifiers['gene'][0]
                except KeyError:
                    methyl['NearestCDS'] = feat.qualifiers['locus_tag'][0]
                if mstart > int(feat.location.end):
                    methyl['NearestCDSrelpo'] = -1 * cdsdist
                else:
                    methyl['NearestCDSrelpo'] = cdsdist
                if feat.location.strand == 1:
                    methyl['NearestCDSft'] = 'Start'
                else:
                    methyl['NearestCDSft'] = 'Stop'
                    methyl['NearestCDSrelpo'] = -1 * methyl['NearestCDSrelpo']

    # Create unique Locus Tags That can be tracked accross isolates, based on relative positions of aligned features.
    if methyl['Strand'] == '+':
        methylstrand = 'plus'
    else:
        methylstrand = 'minus'
    methyl['RefLoci'] = '_'.join([methylstrand, methyl['NearestCDS'], methyl['NearestCDSft']])  # combine into loci
    if methyl['NearestCDSrelpo'] >= 0:
        methyl['RefLoci'] += '_+' + str(methyl['NearestCDSrelpo'])
    else:
        methyl['RefLoci'] += '_' + str(methyl['NearestCDSrelpo'])
    return methyl


def main():
    # call like this:
    # python methylap.py -o 1-0007_methylap.p -n 6
    parser = argparse.ArgumentParser(description='finds TSS near methylated bases',
                                     epilog='Takes in embl file and a preprocessed motifs.gff file')
    parser.add_argument('-o', '--outfile', help='Filename for output', default='1-0007_methylap.p')
    parser.add_argument('-m', '--methfile', help='methyls.csv file of isolate', default='1-0007-methyls.csv')
    parser.add_argument('-e', '--embl', help='Genbank Annotation File',
                        default=GROUPHOME + '/data/annotation/1-0007.gbk')
    parser.add_argument('-a', '--type', help='Annotation File Type', default='genbank')
    parser.add_argument('-s', '--sigfile', help='Sigma Motif File', default='sigmotifs.txt')
    parser.add_argument('-t', '--mtasefile', help='MTase Motif File', default='mtase-motifs.txt')
    parser.add_argument('-g', '--g4file', help='G4-Quad Motif File', default='g4quad-motifs.txt')
    parser.add_argument('-n', '--nproc', help='number of cores', default=6)

    args = parser.parse_args()
    methfile = args.methfile

    # parse csv file with methylation data into list of dicts. keys are based on first row of csv
    methylist1 = []
    with open(methfile) as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            methylist1.append(row)
    # parse files with sigma factor, MTase, and G4-quadruplex motifs into a dictionary of re patterns
    sigmotifs = parsesig(args.sigfile)   # this one is a list of dictionaries, each item is a Sigma Type (a, f, m, etc.)
    mtasemotifs = parsemotif(args.mtasefile, ',')
    g4motifs = parsemotif(args.g4file, ';')
    # parse embl file with isolates sequence, tss features, and csv features
    embl = SeqIO.parse(args.embl, args.type).next()  # read in the embl file, should read only the first contig
    genomesize = len(embl.seq)

    print('Files Read. Searching for Nearby TSS')
    # search the embl file for TSSs near the methylated positions in methylist, multiprocessed
    partialfuntss = partial(findtss, rattembl=embl, genomesize=genomesize)
    pool = Pool(int(args.nproc))
    methylist2 = pool.map(partialfuntss, methylist1)
    pool.close()
    pool.join()

    print('TSS Found, Searching for Nearby Motifs')
    partialfunsig = partial(findsfbs, siglist=sigmotifs, genome=embl.seq, genomesize=genomesize, regionsize=150)
    pool = Pool(int(args.nproc))
    methylist1 = pool.map(partialfunsig, methylist2)
    pool.close()
    pool.join()

    partialfunmt = partial(findmotifs, motlist=mtasemotifs, matchlistkey='AllMTase', genome=embl.seq,
                           genomesize=genomesize, regionsize=150)
    pool = Pool(int(args.nproc))
    methylist2 = pool.map(partialfunmt, methylist1)
    pool.close()
    pool.join()

    partialfung4 = partial(findmotifs, motlist=g4motifs, matchlistkey='AllG4quad', genome=embl.seq,
                           genomesize=genomesize, regionsize=150)
    pool = Pool(int(args.nproc))
    methylist1 = pool.map(partialfung4, methylist2)
    pool.close()
    pool.join()

    print('Motifs Found, Searching for CDS')
    partialfuncds = partial(intracds, rattembl=embl, genomesize=genomesize)
    pool = Pool(int(args.nproc))
    methylist2 = pool.map(partialfuncds, methylist1)
    pool.close()
    pool.join()

    partialfuncdsnear = partial(nearestcds, rattembl=embl, genomesize=genomesize)
    pool = Pool(int(args.nproc))
    methylistfin = pool.map(partialfuncdsnear, methylist2)
    pool.close()
    pool.join()

    print('All Searches Complete, Pickling to:')
    print(args.outfile)
    with open(args.outfile, 'w') as ofile:
        pickle.dump(methylistfin, ofile)



if __name__ == '__main__':
    main()
