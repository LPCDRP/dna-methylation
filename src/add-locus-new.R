# This script is still pretty slow, especially when merging the data tables. I clear out the older variables because of this.
library(reshape)
library(data.table)
library(ggplot2)

# Read in input
file = fread("/grp/valafar/data/methylation/master-methylap-polished-all.csv")
home = "/home/ckim/Projects"

##### SEPARATING FILE INTO FOUR GROUPS: CDS, TSS INTERNAL, TSS DOWNSTREAM, OTHER #####
CDS_data = subset(file, CDS1 != "none")
TSS_int = subset(file, CDS1 == "none" & `TSS internal gene` != "none" & `TSS internal gene` != "TSS_not_internal_to_CDS")
TSS_ds = subset(file, CDS1 == "none")
TSS_ds = subset(TSS_ds, `TSS internal gene` == "none" | `TSS internal gene` == "TSS_not_internal_to_CDS")
TSS_ds = subset(TSS_ds, `TSS downstream gene` != "none" & `TSS downstream gene` != "no_downstream_cds")
other_data = subset(file, CDS1 == "none")
other_data = subset(other_data, `TSS internal gene` == "none" | `TSS internal gene` == "TSS_not_internal_to_CDS")
other_data = subset(other_data, `TSS downstream gene` == "none" | `TSS downstream gene` == "no_downstream_cds")

##### CREATE LOCUS COLUMNS #####
TSS_int$`dist from TSS` = abs(as.numeric(TSS_int$`dist from TSS`))
TSS_ds$`dist from TSS` = abs(as.numeric(TSS_ds$`dist from TSS`))
CDS_data$locus = paste("CDS", CDS_data$CDS1, CDS_data$'position within CDS1', CDS_data$strand)
TSS_int$locus = paste("TSS internal", TSS_int$'TSS internal gene', TSS_int$'dist from TSS', TSS_int$strand)
TSS_ds$locus = paste("TSS downstream", TSS_ds$'TSS downstream gene', TSS_ds$'dist from TSS', TSS_ds$strand)
other_data$locus = "null"

##### MERGING DATA TABLES AND CLEARING OUT OLD DATA TABLES FOR SPACE #####
file = 0
file2 = rbindlist(list(CDS_data, TSS_int, TSS_ds, other_data))
CDS_data = 0
TSS_int = 0
TSS_ds = 0
other_data = 0

##### CLUSTERING #####
test = file2[, c("median", "mean", "std", "locus")]
meanmedian_tbl = test[, .(mean = mean(mean), median = mean(median), std = mean(std), norm_std = mean(std / mean)), by = .(locus)]
meanmedian_tbl = subset(meanmedian_tbl, locus != "null")
meanmedian_tbl = unique(meanmedian_tbl)

# Cluster the data and create plots
means <- na.omit(meanmedian_tbl$mean)
means <- scale(means)
wss <- (nrow(means)-1)*sum(apply(means,2,var))
for (i in 2:15) wss[i] <- sum(kmeans(means, centers=i)$withinss)
plot(1:15, wss, type="b", xlab="Number of Clusters", ylab="Within groups sum of squares") 
fit = kmeans(means, 2) # Change this number to alter the number of clusters
meanmedian_tbl$cluster = fit$cluster

##### OPTIONAL: WRITING TO FILE #####
loclust = meanmedian_tbl[, c("locus", "cluster")]
write.table(loclust, file = "/home/ckim/Projects/loclust-methylap.csv", sep = "\t", quote = FALSE)
# write.table(file2, file = "/grp/valafar/data/methylation/methylap-polished-new.csv", sep = "\t", quote = FALSE)