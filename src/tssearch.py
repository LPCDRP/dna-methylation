#!/usr/bin/env python2.7


def isdownstream(methp, tssp, tstrand, regionsize, genomesize):
    downstream = 0
    if tstrand == 1 and methp < tssp < methp + regionsize:
        downstream = 1
    elif tstrand == 1 and methp < tssp + genomesize < methp + regionsize:
        downstream = 1
    elif tstrand == -1 and tssp < methp < tssp + regionsize:
        downstream = 1
    elif tstrand == -1 and tssp < methp + genomesize < tssp + regionsize:
        downstream = 1
    return downstream


def findcds(tssp, strand, regionsize, record, genomesize):
    nearcdslist = []  # stores candidate cds, candidate cds info stored as dictionaries
    for feat in record.features:
        if feat.type == 'CDS' and feat.location.strand == strand:
            if strand == 1:
                cdsp = int(feat.location.start) + 1  # accounting for python indexing
                if tssp <= cdsp < tssp + regionsize:
                    loctags = feat.qualifiers['locus_tag'][0]  # biopython stores locustag in a one element list.
                    try:
                        genenames = feat.qualifiers['gene'][0]
                    except KeyError:
                        genenames = feat.qualifiers['locus_tag'][0]
                    cdsdict = dict(GeneName=genenames, GeneLT=loctags, GeneStart=cdsp)
                    cdsdict['LeaderRNAbp'] = cdsp - tssp  # if 0 then TSS is leaderless
                    nearcdslist.append(cdsdict)
                elif tssp < cdsp + genomesize < tssp + regionsize:  # circular genome
                    loctags = feat.qualifiers['locus_tag'][0]
                    try:
                        genenames = feat.qualifiers['gene'][0]
                    except KeyError:
                        genenames = feat.qualifiers['locus_tag'][0]
                    cdsdict = dict(GeneName=genenames, GeneLT=loctags, GeneStart=cdsp)
                    cdsdict['LeaderRNAbp'] = cdsp + genomesize - tssp
                    nearcdslist.append(cdsdict)
            else:  # - strand transcribed in opposite direction
                cdsp = int(feat.location.end)  # end location is start of CDS
                if cdsp <= tssp < cdsp + regionsize:
                    loctags = feat.qualifiers['locus_tag'][0]
                    try:
                        genenames = feat.qualifiers['gene'][0]
                    except KeyError:
                        genenames = feat.qualifiers['locus_tag'][0]
                    cdsdict = dict(GeneName=genenames, GeneLT=loctags, GeneStart=cdsp)
                    cdsdict['LeaderRNAbp'] = tssp - cdsp  # if 0 then TSS is leaderless
                    nearcdslist.append(cdsdict)
                elif cdsp < tssp + genomesize < cdsp + regionsize:  # circular genome
                    loctags = feat.qualifiers['locus_tag'][0]
                    try:
                        genenames = feat.qualifiers['gene'][0]
                    except KeyError:
                        genenames = feat.qualifiers['locus_tag'][0]
                    cdsdict = dict(GeneName=genenames, GeneLT=loctags, GeneStart=cdsp)
                    cdsdict['LeaderRNAbp'] = (tssp + genomesize) - cdsp
                    nearcdslist.append(cdsdict)
    if nearcdslist:  # checks if downcdslistfts is not empty, aka checks if there is a downstream cds within regionsize
        cds = min(nearcdslist, key=lambda x: x['LeaderRNAbp'])  # chooses candidate CDS closest to TSS
    else:  # define default values when no downstream CDS
        cds = dict(GeneName='no_downstream_cds', GeneLT='no downstream cds', GeneStart='no_downstream_cds',
                   LeaderRNAbp='no_downstream_cds')
    return cds


def findintracds(tssp, strand, record):
    intracds = False  # stores cds feature the TSS is within, or False if TSS is not inside a CDS
    for feat in record.features:
        if feat.type == 'CDS' and feat.location.strand == strand and tssp in feat:
            intracds = feat
    return intracds


def findtss(methyl, rattembl, genomesize):
    methylstart = int(methyl['Start'])  # convert genome position of methyl from string to integer for calculations

    # find TSS features within 100 bp downstream of the methylated base (care about TSS strand, but not methyl strand)
    tssfeatlist = []  # stores them
    for mrna in rattembl.features:
        mrnastart = int(mrna.location.start)
        if methylstart <= mrnastart < (100 + methylstart) and mrna.location.strand == 1 and mrna.type == 'mRNA':
            tssfeatlist.append(mrna)
        elif methylstart >= mrnastart > (methylstart - 100) and mrna.location.strand != 1 and mrna.type == 'mRNA':
            tssfeatlist.append(mrna)
        # this next condition take into account the circular genome
        elif abs(methylstart-mrnastart) > (genomesize - 100) and mrna.type == 'mRNA':
            tssfeatlist.append(mrna)

    # now extract data from these tss features and determine info for new data structure
    tsslist = []  # stores them
    methyl['TSScount'] = len(tssfeatlist)
    for tss in tssfeatlist:
        tssstart = int(tss.location.start) + 1  # accounting for python counting
        tssdict = dict(TSS=tssstart)
        if tss.location.strand == 1:
            tssdict['TSSstrand'] = '+'
        else:
            tssdict['TSSstrand'] = '-'

        # determine the position of the TSS relative to the Methylated Base
        if abs(tssstart - methylstart) < 105:
            tssdict['TSSrelP'] = tssstart - methylstart
        elif (genomesize - tssstart) < 100:
            tssdict['TSSrelP'] = tssstart - (methylstart + genomesize)
        elif (genomesize - methylstart) < 100:
            tssdict['TSSrelP'] = (tssstart + genomesize) - methylstart
        else:
            tssdict['TSSrelP'] = 'error'

        # find the first downstream cds for these tss. output of function is dictionary with relevant data
        downstreamcds = findcds(tssp=tssstart, strand=tss.location.strand, regionsize=5000, record=rattembl,
                                genomesize=genomesize)
        tssdict['GeneName'] = downstreamcds['GeneName']  # transfer data from found cds
        tssdict['GeneLT'] = downstreamcds['GeneLT']
        tssdict['GeneStart'] = downstreamcds['GeneStart']
        tssdict['LeaderRNAbp'] = downstreamcds['LeaderRNAbp']

        # find a CDS the TSS is inside
        intracds = findintracds(tssp=tssstart, strand=tss.location.strand, record=rattembl)
        if intracds:
            tssdict['IntraCDSLT'] = intracds.qualifiers['locus_tag'][0]
            try:
                tssdict['IntraCDS'] = intracds.qualifiers['gene'][0]
            except KeyError:
                tssdict['IntraCDS'] = intracds.qualifiers['locus_tag'][0]
        else:
            tssdict['IntraCDS'] = 'TSS_not_internal_to_CDS'
            tssdict['IntraCDSLT'] = 'TSS_not_internal_to_CDS'

        # determine region methylated base falls in relative to TSS
        if tssdict['GeneName'] == 'no_downstream_cds':
            tssdict['TSSregion'] = 'elsewhere'
        elif isdownstream(methp=methylstart, tssp=tss.location.start, tstrand=tss.location.strand,
                          regionsize=50, genomesize=genomesize):
            tssdict['TSSregion'] = 'promoter'
        elif isdownstream(methp=methylstart, tssp=tss.location.start, tstrand=tss.location.strand,
                          regionsize=100, genomesize=genomesize):
            tssdict['TSSregion'] = 'downstream_not_prom'
        else:
            tssdict['TSSregion'] = '5pUTR'
        # add growth condition
        tssdict['TSScondition'] = 'Not_Specified'
        if 'Growth' in tss.qualifiers['note'][0]:
            tssdict['TSScondition'] = 'Growth'
        if 'Starvation' in tss.qualifiers['note'][0]:
            tssdict['TSScondition'] = 'Starvation'
        if 'Growth' in tss.qualifiers['note'][0] and 'Starvation' in tss.qualifiers['note'][0]:
            tssdict['TSScondition'] = 'Growth/Starvation'
        # add type
        tssdict['TSStype'] = 'Sense'
        if 'Antisense' in tss.qualifiers['note'][0]:
            tssdict['TSStype'] = 'antisense'
        # and append
        tsslist.append(tssdict)
    if tsslist:  # checks that tsslist is nonempty, aka that there are nearby tss for this methyl
        methyl['AllTSS'] = tsslist
    else:
        methyl['AllTSS'] = False  # default false
    return methyl
