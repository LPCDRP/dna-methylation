mkdir hypo-loci-mama
cd hypo-loci-mama
for tranfact in $(ls $GROUPHOME/resources/TFBS/MEME_OUTPUT/all)
do 
fimo --oc $tranfact -bgfile ../mtbackground --thresh 0.01 $GROUPHOME/resources/TFBS/MEME_OUTPUT/all/$tranfact/meme.txt ../../counts/contexts-hypo-loci-mama.fa
done
cd ..
mkdir hypo-loci-hsdm
cd hypo-loci-hsdm
for tranfact in $(ls $GROUPHOME/resources/TFBS/MEME_OUTPUT/all)
do 
fimo --oc $tranfact -bgfile ../mtbackground --thresh 0.01 $GROUPHOME/resources/TFBS/MEME_OUTPUT/all/$tranfact/meme.txt ../../counts/contexts-hypo-loci-hsdm.fa
done
cd ..
mkdir meth-loci-mama
cd meth-loci-mama
for tranfact in $(ls $GROUPHOME/resources/TFBS/MEME_OUTPUT/all)
do 
fimo --oc $tranfact -bgfile ../mtbackground --thresh 0.01 $GROUPHOME/resources/TFBS/MEME_OUTPUT/all/$tranfact/meme.txt ../../counts/contexts-meth-loci-mama.fa
done
cd ..
mkdir meth-loci-mamb
cd meth-loci-mamb
for tranfact in $(ls $GROUPHOME/resources/TFBS/MEME_OUTPUT/all)
do 
fimo --oc $tranfact -bgfile ../mtbackground --thresh 0.01 $GROUPHOME/resources/TFBS/MEME_OUTPUT/all/$tranfact/meme.txt ../../counts/contexts-meth-loci-mamb.fa
done
cd ..
mkdir meth-loci-hsdm
cd meth-loci-hsdm
for tranfact in $(ls $GROUPHOME/resources/TFBS/MEME_OUTPUT/all)
do 
fimo --oc $tranfact -bgfile ../mtbackground --thresh 0.01 $GROUPHOME/resources/TFBS/MEME_OUTPUT/all/$tranfact/meme.txt ../../counts/contexts-meth-loci-hsdm.fa
done
cd ..
