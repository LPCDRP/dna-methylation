#!/usr/bin/env python2.7
import os
import csv
import argparse
import sqlite3
from sqlite3 import Error

GROUPHOME = os.environ['GROUPHOME']


def main():
    # Build Tables in the master-methylap.db database using precursor plaintext files
    parser = argparse.ArgumentParser(description='Builds Tables in a sqlite3 database using precursor plaintext files')
    parser.add_argument('-d', '--dbf', help='The sqlite3 Database file to store the tables.', default='meth.db')
    parser.add_argument('-i', '--iso', help='File with list of Isolate IDs. 1 Isolate per line',
                        default=GROUPHOME + '/data/methylation/isolates.txt')
    args = parser.parse_args()

    # read MTase Motifs from file
    motifile = GROUPHOME + '/data/methylation/mt-motifs.tsv'
    motiflistdict = []  # stores a list of dictionaries. dicts keyed by MTase, Motif, and Palindrome
    with open(motifile, 'r') as f:
        reader = csv.DictReader(f, delimiter='\t')
        for row in reader:
            motiflistdict.append(row)

    # find every isolate with a complete assembly in our PacBio Sequence assembly pipeline
    # isolist = []  # list to store isolate names
    # for name in os.listdir(GROUPHOME + '/data/genomes/'):
    #     if name.endswith('.fasta') and 'uninverted' not in name:
    #         # completed isolates each have a file with a .fasta extension
    #         # also need to remove the 'uninverted' files made for another project
    #         isolatename = name.split('.')[0]  # remove file extension from isolate name
    #         isolist.append(isolatename)
    # print('Found ' + str(len(isolist)) + ' isolates')
    # isolist.append('2-0043')  # this isolate is called 2-0043-unknown in data/genomes [may need to be removed from db]
    # isolist.append('2-0045')  # this isolate failed assembly, but has entries I am keeping in db - for now
    print(args.iso)
    isolist = []  # list to store isolate names
    with open(args.iso, 'r') as f:
        for line in f:
            cleanline = line.strip('\n')
            isolist.append(cleanline)

    # read lineage data from file
    lineagefile = GROUPHOME + '/metadata/lineage.txt'
    lineagedict = {}  # dictionary. key is isolate and value is lineage
    with open(lineagefile, 'r') as linfile:
        next(linfile)
        for line in linfile:
            cleanline = line.strip('\n')
            linebits = cleanline.split('\t')
            lineagedict[linebits[0]] = linebits[1]

    # read dst data from file
    dstfile = GROUPHOME + '/metadata/dst.txt'
    dstdictdict = {}  # to store a dictionary of dictionaries. key is isolate, value is dict keyed by inh, rif, etc.
    with open(dstfile, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            isolate = row['isolate']
            dstdictdict[isolate] = row

    # read MTase Genotypes File
    mtgtfile = GROUPHOME + '/data/methylation/mtase_variants.tsv'
    mtgtlistdict = []  # to store a list of dictionaries. Dicts keyed by Isolate, Genotype, Gene, and MTase
    with open(mtgtfile, 'r') as f:
        reader = csv.DictReader(f, delimiter='\t')
        for row in reader:
            mtgtlistdict.append(row)

    # read MTase Mutation Activity File (gives methylation activity of each unique mutation found in MTase genes)
    mutfile = GROUPHOME + '/data/methylation/mt-mutation-activity.txt'
    mutlistdict = []  # to store a list of dictionaries. dicts keyed by MTase, Gene, Genotype, and Activity
    with open(mutfile, 'r') as f:
        reader = csv.DictReader(f, delimiter='\t')
        for row in reader:
            mutlistdict.append(row)

    # connect to the database (will create one if it doesn't already exist)
    try:
        conn = sqlite3.connect(args.dbf)  # connects to database. SQLite makes one if it doesnt exist
        print(sqlite3.version)
    except Error as e:
        print('ERROR:')
        print(e)

    # now add a table
    # these sqlite commands create tables and assign their column names
    tb_motif = """CREATE TABLE IF NOT EXISTS Motifs (
                                        MTaseID integer PRIMARY KEY,
                                        MTase text NOT NULL,
                                        Motif text NOT NULL,
                                        Palindrome text NOT NULL
                                    );"""

    tb_metadata = """ CREATE TABLE IF NOT EXISTS Metadata (
                                        IsolateID integer PRIMARY KEY,
                                        Isolate text NOT NULL,
                                        Lineage text NOT NULL,
                                        INH text NOT NULL,
                                        RIF text NOT NULL,
                                        AMK text NOT NULL,
                                        CAP text NOT NULL,
                                        KAN text NOT NULL,
                                        MOX text NOT NULL,
                                        OFX text NOT NULL,
                                        FQ text NOT NULL,
                                        PZA text NOT NULL,
                                        Resistance text NOT NULL
                                    ); """

    tb_mtase = """CREATE TABLE IF NOT EXISTS MTase (
                                        id integer PRIMARY KEY,
                                        Isolate text NOT NULL,
                                        Genotype text NOT NULL,
                                        Gene text NOT NULL,
                                        MTase text NOT NULL,
                                        IsolateID integer NOT NULL,
                                        MTaseID integer NOT NULL,
                                        FOREIGN KEY (IsolateID) REFERENCES Metadata (IsolateID),
                                        FOREIGN KEY (MTaseID) REFERENCES Motifs (MTaseID)
                                    );"""

    tb_mutations = """CREATE TABLE IF NOT EXISTS Mutations (
                                        MutationID integer PRIMARY KEY,
                                        MTase text NOT NULL,
                                        Gene text NOT NULL,
                                        Genotype text NOT NULL,
                                        Activity text NOT NULL,
                                        MTaseID integer NOT NULL,
                                        FOREIGN KEY (MTaseID) REFERENCES Motifs (MTaseID)
                                    );"""
    try:
        cur = conn.cursor()
        cur.execute(tb_motif)
        cur.execute(tb_metadata)
        cur.execute(tb_mtase)
        cur.execute(tb_mutations)
        print('tables made')
    except Error as e:
        print('ERROR:')
        print(e)

    conn.close()

    # now add data to the table
    conn = sqlite3.connect(args.dbf)
    with conn:
        cur = conn.cursor()
        motifinsert = ''' INSERT INTO Motifs(MTase, Motif, Palindrome)
                                          VALUES(?,?,?) '''
        metadatainsert = ''' INSERT INTO Metadata(Isolate, Lineage, INH, RIF, AMK, CAP, KAN, MOX, OFX, FQ, PZA, Resistance)
                          VALUES(?,?,?,?,?,?,?,?,?,?,?,?) '''
        mtaseinsert = ''' INSERT INTO MTase(Isolate, Genotype, Gene, MTase, IsolateID, MTaseID)
                                  VALUES(?,?,?,?,?,?) '''
        mutinsert = ''' INSERT INTO Mutations(MTase, Gene, Genotype, Activity, MTaseID)
                                          VALUES(?,?,?,?,?) '''

        # Fill Motifs table with data from mt-motifs.tsv
        mtkeys = {}  # dictionary stores MTaseID in database, keyed by MTase name
        for entry in motiflistdict:
            motifrow = (entry['MTase'], entry['Motif'], entry['Palindrome'])
            cur.execute(motifinsert, motifrow)  # insert row using insert command and row data
            mtkeys[entry['MTase']] = cur.lastrowid  # save MTaseID, the integer primary key of the Motifs table

        # Fill Metadata table with data from dst and lineage files.
        isolatekeys = {}  # dictionary stores IsolateID in database of each Isolate, keyed by Isolate name
        for isolate in isolist:  # include only isolates in isolist
            # Fill in fields if isolate is not in those files
            try:
                dstdict = dstdictdict[isolate]
            except KeyError as e:
                dstdict = dict(inh='ND', rif='ND', amk='ND', cap='ND', kan='ND', mox='ND', ofx='ND', fq='ND', pza='ND',
                               resistance='ND')
            try:
                lineage = lineagedict[isolate]
            except KeyError as e:
                lineage = 'NA'
            metadatarow = (isolate, lineage, dstdict['inh'], dstdict['rif'], dstdict['amk'], dstdict['cap'],
                           dstdict['kan'], dstdict['mox'], dstdict['ofx'], dstdict['fq'], dstdict['pza'],
                           dstdict['resistance'])
            cur.execute(metadatainsert, metadatarow)  # insert row into Metadata table using command and row data
            isolatekeys[isolate] = cur.lastrowid  # save IsolateID, the integer primary key of the Metadata table

        # Fill MTase table with data from mtase_variants.tsv
        for entry in mtgtlistdict:
            isolate = entry['Isolate']
            try:
                isolate_id = isolatekeys[isolate]
                mtase_id = mtkeys[entry['MTase']]  # retrieve the MTaseID for this MTase
                mtaserow = (isolate, entry['Genotype'], entry['Gene'], entry['MTase'], isolate_id, mtase_id)
                cur.execute(mtaseinsert, mtaserow)
            except KeyError as e:
                print('Isolate in mtase_variants.tsv has no fasta file in GROUPHOME/data/genomes')
                print(e)


        # Fill Mutations table with data from mt-mutation-activity.txt
        for entry in mutlistdict:
            mtase_id = mtkeys[entry['MTase']]
            mutrow = (entry['MTase'], entry['Gene'], entry['Genotype'], entry['Activity'], mtase_id)
            cur.execute(mutinsert, mutrow)

    # and close the connection to the database before finishing
    conn.close()


if __name__ == '__main__':
    main()
