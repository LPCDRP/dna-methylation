library(ggplot2)
library(data.table)

# Change path to where you want the plots to be saved
home = "/home/ckim/Projects/dna-methylation"
file = fread("/grp/valafar/data/methylation/counts/meth-g4-per-isolate.csv")

# Creates a bar graph with five bars for each of the lineages; each bar is the mean count for each G4 type
dt = file[, .(L3N4D7 = mean(l3n4d7), L3N4D11 = mean(l3n4d11), L4N4D15 = mean(l4n4d15), L3N4D15 = mean(l3n4d15), L4N4D7 = mean(l4n4d7)), by = "lineage"]
mdata = melt(dt, id.vars = "lineage")
names(mdata) = c("lineage", "G4_type", "count_overlapping_quadruplexes")
g4plot = ggplot(mdata, aes(lineage, count_overlapping_quadruplexes)) + geom_bar(aes(fill = G4_type), position = "dodge", stat = "identity") + theme(axis.text.x = element_text(angle = 45, hjust = 1))

# Creates five bar graphs, one for each lineage. Isolates are in descending order by number of overlapping G4 quadruplexes of that type
dt2 = file[, .(L3N4D7_count = l3n4d7, L3N4D11_count = l3n4d11, L4N4D15_count = l4n4d15, L3N4D15_count = l3n4d15, L4N4D7_count = l4n4d7), by = c("isolate","lineage")]
l3n4d7_plot = ggplot(dt2, aes(x = isolate, y = L3N4D7_count)) + geom_bar(aes(reorder(isolate,-L3N4D7_count), fill = lineage), position = "dodge", stat = "identity") + theme(text = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1)) + ggtitle("Number of Overlapping G4 Quadruplexes per Isolate")
l3n4d11_plot = ggplot(dt2, aes(x = isolate, y = L3N4D11_count)) + geom_bar(aes(reorder(isolate,-L3N4D11_count), fill = lineage), position = "dodge", stat = "identity") + theme(text = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1)) + ggtitle("Number of Overlapping G4 Quadruplexes per Isolate")
l4n4d15_plot = ggplot(dt2, aes(x = isolate, y = L4N4D15_count)) + geom_bar(aes(reorder(isolate,-L4N4D15_count), fill = lineage), position = "dodge", stat = "identity") + theme(text = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1)) + ggtitle("Number of Overlapping G4 Quadruplexes per Isolate")
l3n4d15_plot = ggplot(dt2, aes(x = isolate, y = L3N4D15_count)) + geom_bar(aes(reorder(isolate,-L3N4D15_count), fill = lineage), position = "dodge", stat = "identity") + theme(text = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1)) + ggtitle("Number of Overlapping G4 Quadruplexes per Isolate")
l4n4d7_plot = ggplot(dt2, aes(x = isolate, y = L4N4D7_count)) + geom_bar(aes(reorder(isolate,-L4N4D7_count), fill = lineage), position = "dodge", stat = "identity") + theme(text = element_text(size = 10), axis.text.x = element_text(angle = 45, hjust = 1)) + ggtitle("Number of Overlapping G4 Quadruplexes per Isolate")

ggsave(filename = "G4-average-count.jpeg", plot = g4plot, path = home, dpi = 300)
ggsave(filename = "L3N4D7-count.jpeg", plot = l3n4d7_plot, path = home, width = 8, dpi = 300)
ggsave(filename = "L3N4D11-count.jpeg", plot = l3n4d11_plot, path = home, width = 8, dpi = 300)
ggsave(filename = "L4N4D15-count.jpeg", plot = l4n4d15_plot, path = home, width = 8, dpi = 300)
ggsave(filename = "L3N4D15-count.jpeg", plot = l3n4d15_plot, path = home, width = 8, dpi = 300)
ggsave(filename = "L4N4D7-count.jpeg", plot = l4n4d7_plot, path = home, width = 8, dpi = 300)